<?php
/*
'{"Error":"3000", "Message":"You must pass in user data to filter on"}');
'{"Error":"3001", "Message":"Appropriate user keys not passed in"}');
'{"Error":"3002", "Message":'. $e->getMessage() .'}';
'{"Error":"3003", "Message":"Authentication failed"}');

*/

	
    require 'Slim/Slim.php';
    require '../phpini.php';
	
	\Slim\Slim::registerAutoloader();
	
	$app = new \Slim\Slim();

	//location of download file
	$app->post('/Secure/Rajam/Get/lodf', 'locationOfDownloadFile');

	/*
	################################################
	##################### USERS ####################
	################################################
	*/
	
	//{"firstname":"", "lastname":"", "username":"", "password":""}	
	$app->post('/Secure/Rajam/Create/User', 'createUser'); 	
	
	//{"username":"", "password":""}
	$app->post('/Secure/Rajam/Authenticate/User', 'authenticateUser');
	
	//none	
	$app->post('/Secure/Rajam/Get/Users', 'retrieveAllUsers');
	
	
	//({"id":"" && ("firstname":""||"lastname":""||"username":""||"password":"")}
	$app->post('/Secure/Rajam/Edit/User', 'editUsers');
			
			$app->post('/Secure/Rajam/Get/UserForUDID', 'getUserForIpadWithUDID');

	/*
	###############################################
	#################### AGENTS ###################
	###############################################
	*/
	
	//({"firstname":"", "lastname":"", "stateid":"", "balance":"", "percentageDiscount":""}
	$app->post('/Secure/Rajam/Create/Agent', 'addAgent');

	//({"id":"" && ("firstname":""||"lastname":""||"stateid":""||"balance":""||"percentageDiscount":"")}
	$app->post('/Secure/Rajam/Edit/Agent', 'editAgent');
	
	//none	
	$app->post('/Secure/Rajam/Get/Agents', 'retrieveAllAgents');
	
	$app->post('/Secure/Rajam/Delete/Agents', 'deleteAgents');
	
	//{"agentid":""};
	$app->post('/Secure/Rajam/Get/Agent', 'retrieveAgent');
	/*
	################################################
	################### FACTORIES ##################
	################################################
	*/
	
	//({"factoryname":"", "stateid":""}
	$app->post('/Secure/Rajam/Create/Factory', 'addFactory');

	//({"id":"" && ("factoryname":""||"stateid":"")}
	$app->post('/Secure/Rajam/Edit/Factory', 'editFactory');
	
	//none	
	$app->post('/Secure/Rajam/Get/Factories', 'retrieveAllFactories');

	//({"factoriestodelete":"")}
	$app->post('/Secure/Rajam/Delete/Factories', 'deleteFactories');

	/*
	################################################
	##################### STATE ####################
	################################################
	*/
	
	//({"statename":"", "abbreviation":""}
	$app->post('/Secure/Rajam/Create/State', 'addState');

	//({"id":"" && ("statename":""||"abbreviation":"")}
	$app->post('/Secure/Rajam/Edit/State', 'editState');
	
	//none	
	$app->post('/Secure/Rajam/Get/States', 'retrieveAllStates');
	//none	
	$app->post('/Secure/Rajam/Delete/States', 'deleteStates');


	/*
	###############################################
	################### PRODUCTS ##################
	###############################################
	*/
	
	//({"firstname":"", "lastname":"", "stateid":"", "balance":"", "percentageDiscount":""}
	$app->post('/Secure/Rajam/Create/Product', 'addProduct');

	/*
//({"id":"" && ("firstname":""||"lastname":""||"stateid":""||"balance":""||"percentageDiscount":"")}
	$app->post('/Secure/Rajam/Edit/Agent', 'editAgent');
*/
	
	//none	
	$app->post('/Secure/Rajam/Get/Products', 'retrieveAllProducts');
	
/* 	$app->post('/Secure/Rajam/Delete/Agents', 'deleteAgents'); */


	$app->post('/Secure/Rajam/Get/AllProductPrices', 'retrieveAllProductsPrices');
	
	//{"productid":"", "stateid":"", "price":""}
	$app->post('/Secure/Rajam/Edit/UpdatePrice', 'updatePrice');
	
	$app->post('/Secure/Rajam/Get/ProductBoxDiscount', 'retrieveBoxDiscountForAgent');
	
	$prefix = "/Secure/Rajam/";
	$app->post('/Secure/Rajam/Update/ProductBoxDiscount', 'updateProductBoxDiscounts');
	
	function locationOfDownloadFile(){
		echo "http://www.pavankataria.com/rajam/";
	}
	
	/*
	###############################################
	#################### ORDERS ###################
	###############################################
	*/
	
	//({"firstname":"", "lastname":"", "stateid":"", "balance":"", "percentageDiscount":""}
	$app->post('/Secure/Rajam/Create/Order', 'createOrder');


//State ONE
	//none
	$app->post('/Secure/Rajam/Get/OrdersStateOne', 'retrieveAllOrdersForPhaseOne_FactoryPlanning');

	//{"orderstoupdatetostatetwo":""}
	$app->post('/Secure/Rajam/Edit/OrdersToStateTwo', 'updateOrdersToStateTwo');
	
//State TWO
	//{"factoryid":""}
	$app->post('/Secure/Rajam/Get/OrdersStateTwo', 'retrieveAllOrdersForPhaseTwo_FactoryPlanning');
	
	//none
	$app->post('/Secure/Rajam/Get/OrdersStateTwoViewing', 'retrieveAllOrdersForPhaseTwo_FactoryViewing');
	
	//{"orderstoupdatetostatetwo":""}
	$app->post('/Secure/Rajam/Edit/OrdersStateTwoToDispatched', 'updateOrdersWithStateTwoToDispatched');
	

	$app->post('/Secure/Rajam/Get/OrdersStateThree', 'retrieveAllOrdersForPhaseThree_CEOConfirmDispatch');
	
	//{"orderstoupdatetostatethree":""}
	$app->post('/Secure/Rajam/Edit/OrdersToStateFour', 'updateOrdersToStateFour');


	$app->post('/Secure/Rajam/Edit/DeleteOrderStateOne', 'deleteOrderStateOne');
	
	$app->post('/Secure/Rajam/Get/OrdersForEdit', 'retrieveAllOrdersForEditOrders');
	
	$app->post('/Secure/Rajam/Update/ExistingOrder', 'updateExistingOrder');
	
	
	$app->post('/Secure/Rajam/Update/SkipOrderToFinalise', 'updateOrdersSkipStraightToFinaliseScreen');
	
	
	$app->post('/Secure/Rajam/Update/ResetOrder','updateOrderReset');
	
	
	//{"factoryid":"", "dispatched":""}

	$app->post('/Secure/Rajam/Get/FactoryOrders', 'retrieveFactoryDonePage');
	
	
	
	
	
	$app->post('/Secure/Rajam/Get/OrdersForCEOPage', 'retrieveAllOrdersForCEOFinalise');
	$app->post('/Secure/Rajam/Edit/FinaliseOrder', 'finaliseOrderForCEO');
	/*
	################################################
	#################### PAYMENT ###################
	################################################
	*/
	
	//({"firstname":"", "lastname":"", "stateid":"", "balance":"", "percentageDiscount":""}
	$app->post('/Secure/Rajam/Create/Order', 'createOrder');

	//none
	$app->post('/Secure/Rajam/Create/Payment', 'createPayment');

	//{"id":""}
	$app->post('/Secure/Rajam/Get/AgentAccounts', 'retrieveAllAgentAccounts');



/*
	//{"orderstoupdatetostatetwo":""}
	$app->post('/Secure/Rajam/Edit/OrdersToStateTwo', 'updateOrdersToStateTwo');
	
//State TWO
	//{"factoryid":""}
	$app->post('/Secure/Rajam/Get/OrdersStateTwo', 'retrieveAllOrdersForPhaseTwo_FactoryPlanning');
	
	//none
	$app->post('/Secure/Rajam/Get/OrdersStateTwoViewing', 'retrieveAllOrdersForPhaseTwo_FactoryViewing');
	
	//{"orderstoupdatetostatetwo":""}
	$app->post('/Secure/Rajam/Edit/OrdersStateTwoToDispatched', 'updateOrdersWithStateTwoToDispatched');
	
	
	$app->post('/Secure/Rajam/Get/OrdersStateThree', 'retrieveAllOrdersForPhaseThree_CEOConfirmDispatch');
	
	//{"orderstoupdatetostatethree":""}
	$app->post('/Secure/Rajam/Edit/OrdersToStateFour', 'updateOrdersToStateFour');
*/



	
	include('users.php');
	
	include('agents.php');
	
	include('factories.php');
	
	include('states.php');

	include('products.php');

	include('orders.php');
	
	include('a.php');
	
	include('p.php');
	
/*
		
	
	
	
	$app->post('/person/:id', function ($id) {
	
		$parameters = json_decode($id);
		echo "parameters: " . $parameters;
	    //Update Person identified by $id
	});
*/


	function getConnection() {
/*
	    $dbhost="127.0.0.1";
	    $dbuser="root";
	    $dbpass="";
	    $dbname="cellar";
*/

		$host = 'localhost';
		$user = "rajamuser";
		$pass = "J7JBtzdwYD2EHyaN";
		$database = 'Rajam';


	    $dbh = new PDO("mysql:host=$host;dbname=$database", $user , $pass);
	    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	    
	    
	    /*
$con = mysql_connect($host, $user, $pass);
	    mysql_select_db($database, $con);
	    
*/
	    return $dbh;
	}
	$app->run();
	
	
		
	
	// Helper method to get a string description for an HTTP status code
// From http://www.gen-x-design.com/archives/create-a-rest-api-with-php/ 
function getStatusCodeMessage($status)
{
    // these could be stored in a .ini file and loaded
    // via parse_ini_file()... however, this will suffice
    // for an example
    $codes = Array(
        100 => 'Continue',
        101 => 'Switching Protocols',
        200 => 'OK',
        201 => 'Created',
        202 => 'Accepted',
        203 => 'Non-Authoritative Information',
        204 => 'No Content',
        205 => 'Reset Content',
        206 => 'Partial Content',
        300 => 'Multiple Choices',
        301 => 'Moved Permanently',
        302 => 'Found',
        303 => 'See Other',
        304 => 'Not Modified',
        305 => 'Use Proxy',
        306 => '(Unused)',
        307 => 'Temporary Redirect',
        400 => 'Bad Request',
        401 => 'Unauthorized',
        402 => 'Payment Required',
        403 => 'Forbidden',
        404 => 'Not Found',
        405 => 'Method Not Allowed',
        406 => 'Not Acceptable',
        407 => 'Proxy Authentication Required',
        408 => 'Request Timeout',
        409 => 'Conflict',
        410 => 'Gone',
        411 => 'Length Required',
        412 => 'Precondition Failed',
        413 => 'Request Entity Too Large',
        414 => 'Request-URI Too Long',
        415 => 'Unsupported Media Type',
        416 => 'Requested Range Not Satisfiable',
        417 => 'Expectation Failed',
        500 => 'Internal Server Error',
        501 => 'Not Implemented',
        502 => 'Bad Gateway',
        503 => 'Service Unavailable',
        504 => 'Gateway Timeout',
        505 => 'HTTP Version Not Supported'
    );
 
    return (isset($codes[$status])) ? $codes[$status] : '';
}
 
// Helper method to send a HTTP response code/message
function sendResponse($status = 200, $body = '', $content_type = 'text/html')
{

    $status_header = 'HTTP/1.1 ' . $status . ' ' . getStatusCodeMessage($status);
    header($status_header);
    header('Status: '. $status .' ' . getStatusCodeMessage($status));
    header('StatusCode: '. $status);

    //header("HTTP/1.1 404 Not Found");
    
    //header('The goggles, they do nawtink!', true, 404);
    header('Content-type: ' . $content_type);
    echo $body;
}	



	function getJSONDecodedPostData(){
    	$request = \Slim\Slim::getInstance()->request();
    	$postData = $request->getBody();
    	$result = json_decode($postData);
    	return $result;
    }
    
		/*
		    		//sendResponse(400, '{"Error":"3000", ""Message":"You must pass in user data to filter on"}');$this->response['Allow'] = implode(' ', $httpMethodsAllowed);
            //$app->halt(405, 'HTTP method not allowed for the requested resource. Use one of these instead:');

/*     		header('Status: 500 Error'); *
  			//$app->contentType('text/html');
   		 	//$app->response()->status(400);
    		//$app->response()->body("lol");
    		//$app->halt(403, 'You shall not pass!');
		
		
		
$db = getConnection();
			$stmt = $db->query($sql);
    		//$stmt->fetchAll(PDO::FETCH_OBJ);

			$stmt -> fetch();	

 			//$result = mysql_query($sql); 			
        	echo '{"wine": ' . $stmt['State_Name'] . '}';
*/
?>


