<?php

	function createOrder(){
	    $result = getJSONDecodedPostData();
    	
    	if($result != NULL){
			if(array_key_exists('orderobject', $result)){
				
				date_default_timezone_set('Asia/Calcutta');

				$script_tz = date_default_timezone_get();

	/*
			if (strcmp($script_tz, ini_get('date.timezone'))){
				    echo 'Script timezone differs from ini-set timezone.';
				} else {
				    echo 'Script timezone and ini-set timezone match.';
				}
*/
				
				$orderobject = $result->orderobject;
				
				//AGENT ID
				$agentId = $orderobject->agentid;
				
				//AGENT NAME
				$agentName = $orderobject->agentname;
				
				//AGENT ORDER COUNT
				$orderCount = $orderobject->agentordercount;
				
				//{"quantityperbox":"80","quantitysold":"10","productname":"2.50 W - 70g","productprice":"130"}
				//ARRAY OF ITEMS
				$invoiceItems = $orderobject->invoiceitems;
				
				//COUNT OF INVOICE ITEMS
				$countOfInvoiceItems = count($invoiceItems);
				
				
				
				//CREATE ORDER REFERENCE NUMBER
				$day = date("d"); 
				$month = date("m"); 
				$counter = str_pad(($orderCount+1), 3, '0', STR_PAD_LEFT);
				
				$referenceCode = str_replace(" ", "_", $agentName) . "/". $day . $month . "/" . $counter;
				
				//$date = date('d/m/Y h:i:s A');
				$date = date('Y-m-d H:i:s');
				

				try{
					$db = getConnection();
					$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    				/* Begin a transaction, turning off autocommit */
					$db->beginTransaction();
					
    				$stmt = $db->prepare('INSERT INTO Orders VALUES(\'\', :ref, :date, \'\', :aid, 1, 0, 0, 0, 0)');
    				$stmt->bindParam(':ref', $referenceCode);
    				$stmt->bindParam(':date', $date);
        			$stmt->bindParam(':aid', $agentId);
    				$stmt->execute();
    				$orderId = $db->lastInsertId(); 
    				
    				
					$stmt = $db->prepare('UPDATE Agents SET Agent_OrderCount = :cow WHERE Agent_Id = :aid');
    				$stmt->bindParam(':cow', $counter);
    				$stmt->bindParam(':aid', $agentId);
    				$stmt->execute();
    				
    				$stmt = $db->prepare('SELECT * FROM PBoxDiscounts WHERE Agent_Id = :aid');
    				$stmt->bindParam(':aid', $agentId);

    				$stmt->execute();
    				
    				$PBoxDiscountsArray = $stmt->fetchAll(PDO::FETCH_ASSOC);

    				
    				
    				$totalPriceForOrder = 0;
    				$totalBoxDiscount = 0;
    				for($i = 0; $i < $countOfInvoiceItems; $i++){
    				
    					
	    				$stmt = $db->prepare('INSERT INTO OrderLines VALUES(\'\', :oid, :aod, :pid, :pn, :pp, :pbd, :qs, :qpb, \'\', \'\')');
	    				$stmt->bindParam(':oid', $orderId);
	    				$stmt->bindParam(':aod', $agentId);
	    				
	    				//check for all products the customer has box discounts for if any
	    				$finalBoxDiscount = 0;

						$currentBoxDiscount = 0;
	    				for($j = 0; $j < count($PBoxDiscountsArray); $j++){
	    					//if the agent does have a box discount.. check the product id and grab the one that matches the product id in the current invoice item
	    					if($PBoxDiscountsArray[$j]['Product_Id'] == $invoiceItems[$i]->productid){
/* 	    						echo "ahahahaha              PBoxDiscountsArray[j]['Product_Id'] = " . $PBoxDiscountsArray[$j]['Product_Id'] .  " and $invoiceItems[$i]->productid = "; */
	    						$currentBoxDiscount = $PBoxDiscountsArray[$j]['PBoxDiscount_RupeesOffForBox'];
	    						$finalBoxDiscount += $currentBoxDiscount;
	    						break;
	    					}
	    				}
	    				$stmt->bindParam(':pid', $invoiceItems[$i]->productid);
	    				$stmt->bindParam(':pn', $invoiceItems[$i]->productname);
	    				$stmt->bindParam(':pp', $invoiceItems[$i]->productprice);
	    				$stmt->bindParam(':pbd', $currentBoxDiscount);

	    				$stmt->bindParam(':qs', $invoiceItems[$i]->quantitysold);
	    				$stmt->bindParam(':qpb', $invoiceItems[$i]->quantityperbox);
	    				
	    				$stmt->execute();
						$totalBoxDiscount += $currentBoxDiscount * $invoiceItems[$i]->quantitysold;
/*     					$totalOrderLevelBalance += ($invoiceItems[$i]->productprice - $currentBoxDiscount) * $invoiceItems[$i]->quantitysold; */

    					$totalPriceForOrder += $invoiceItems[$i]->productprice * $invoiceItems[$i]->quantitysold;
    					/*
							echo '\n{"orderid":"' . $orderId . '"}';
							echo '\n{"agentid":"' . $agentId . '"}';
							echo '\n{"productname":"' . $invoiceItems[$i]->productname . '"}';
							echo '\n{"productprice":"' . $invoiceItems[$i]->productprice . '"}';
							echo '\n{"quantitysold":"' . $invoiceItems[$i]->quantitysold . '"}';
							echo '\n{"qpb":"' . $invoiceItems[$i]->quantityperbox . '"}';
						*/

					}    				
  					
  					/* 	    				$stmt->bindParam(':olb', $totalPriceForProduct);	    				 */
					
					$boxDiscountDeductedFromTotalAmount = $totalPriceForOrder-$totalBoxDiscount;
					
					
					$stmt = $db->prepare('SELECT * FROM Agents WHERE Agent_Id = :aid');
					$stmt->bindParam(':aid', $agentId);
					$stmt->execute();
					$result2 = $stmt->fetchAll(PDO::FETCH_ASSOC);
					
					$agentPercentageDiscount = (min(max(intval($result2[0]['Agent_PercentageDiscount']), 0), 99));
					
/*
					if($agentPercentageDiscount == 0){
						$agentPercentageDiscount = 0;
					}
*/
/*
					$totalPercentageDiscount = round(($boxDiscountDeductedFromTotalAmount * $agentPercentageDiscount)/100);
					
					$totalPriceForOrder = $boxDiscountDeductedFromTotalAmount - $totalPercentageDiscount;
		
    				
					$stmt = $db->prepare('UPDATE Orders SET Order_LevelBalance = :old, Order_TotalAmount = :told, Order_TotalBoxDiscount = :tbld, Order_TotalPercentageDiscount = :pd  WHERE Order_ReferenceNumber = :orn');
    				$stmt->bindParam(':tbld', $totalBoxDiscount);
    				
    				$stmt->bindParam(':old', $totalPriceForOrder);
    				$stmt->bindParam(':pd', $totalPercentageDiscount);
    				$stmt->bindParam(':told', $totalPriceForOrder);

    				$stmt->bindParam(':orn', $referenceCode);
    				$stmt->execute();
*/

    				$db->commit();
    				
    				
    				/*
if($result->comment != "none"){
    					$stmt = $db->prepare('SELECT Agent_Id FROM Agents WHERE Agent_Name = :an');
    					$stmt->bindParam(':an', $result->name);
    					$stmt->execute();
    					
    					$result2 = $stmt->fetchAll(PDO::FETCH_ASSOC);
    					echo $result2[0]['Agent_Id'] . "\n";
    				}
    				$stmt = $db->prepare('INSERT INTO AgentNotes (AgentNote_Id, Agent_Id, AgentNotes_Comment)
	    				VALUES(NULL, :id, :com)
	    				ON DUPLICATE KEY UPDATE
	    				AgentNotes_Comment = :com');
						$stmt->bindParam(':id', $result2[0]['Agent_Id']);
						$stmt->bindParam(':com', $result->comment);
	    				$stmt->execute();
*/
/* 					echo $referenceCode; */
					sendResponse(200, '{"Error":"0", "Message":"Successfully created agent", "OrderReferenceNumber":"' . $referenceCode . '"}');
    				return;
    			}
		    	catch(PDOException $e){
		    	/* Recognize mistake and roll back changes */
					$db->rollBack();
    				sendResponse(400, '{"Error":"3002", "Message":"'. $e->getMessage() .'"}');
    				return;
    			}

			}
			sendResponse(400, '{"Error":"3001", "Message":"Appropriate \'order\' keys not passed in"}');
	   		return;
    	}
    	sendResponse(400, '{"Error":"3000", "Message":"You must pass in \'order\' data to filter on"}');
    	return;
   }




	function retrieveAllOrdersForPhaseThree_CEOConfirmDispatch(){
		try{
			$db = getConnection();
			
			$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    		/* Begin a transaction, turning off autocommit */
    		$db->beginTransaction();
					

			$stmt = $db->prepare('SELECT * FROM Orders WHERE (Order_State = 3 OR Order_State = 2)'); 
			$stmt->execute();

			$result1 = $stmt->fetchAll(PDO::FETCH_ASSOC);
			
			/*
$stmt = $db->prepare('SELECT a.Order_Id, a.Agent_Id, a.Order_CreatedDate, a.Order_State, a.Order_ReferenceNumber, 
								
								b.OrderLine_Id, 
								b.OrderLine_ProductNameATOP, 
								b.OrderLine_PriceOfProductATOP,
								b.OrderLine_PBoxDiscountATOP, 
								b.OrderLine_QuantitySoldATOP, 
								b.OrderLine_QuantityPerBoxATOP, 
								b.OrderLine_AssignedToFactoryId, 
								b.OrderLine_Dispatched
								
								FROM Orders a, OrderLines b
								WHERE 
									a.Order_Id = b.Order_Id AND 
									a.Order_State = 3');
*/



		//echo "order ref " . $result1[0]['Order_ReferenceNumber'];// . " agent id " . $result1[0]['Agent_Id']; 

		









			$stmt = $db->prepare('	SELECT 
										a.Order_Id, 
										a.Agent_Id, 
										c.Agent_Balance, 
										a.Order_CreatedDate, 
										a.Order_State, 
										a.Order_ReferenceNumber, 
										b.OrderLine_Id, 
										b.OrderLine_ProductNameATOP, 
										b.OrderLine_PriceOfProductATOP, 
										b.OrderLine_PBoxDiscountATOP, 
										b.OrderLine_QuantitySoldATOP, 
										b.OrderLine_QuantityPerBoxATOP, 
										b.OrderLine_AssignedToFactoryId, 
										b.OrderLine_Dispatched
									FROM 
										Orders a, 
										OrderLines b, 
										Agents c
									WHERE 
										a.Order_Id = b.Order_Id
									AND 
										a.Order_State =3
									AND 
										a.Agent_Id = c.Agent_Id');				

			$stmt->execute();
			$result2 = $stmt->fetchAll(PDO::FETCH_ASSOC);
			
			
			
				
			$stmt = $db->prepare('SELECT * FROM Factories');
			$stmt->execute();
			$result3 = $stmt->fetchAll(PDO::FETCH_ASSOC);


			$db->commit();
					
			$array = array(
				"orderResults"=>$result1,
				"detailedOrderResults"=>$result2,
				"factoriesResult"=>$result3			
			);
			
			
			
			sendResponse(200, json_encode($array));
		
		}
		catch(PDOException $e){
			$db->rollBack();

			sendResponse(400, '{"Error":"3002", "Message":"'. $e->getMessage() .'"}');
		}
	}
	

	
	function retrieveAllOrdersForCEOFinalise(){

		try{
			$db = getConnection();
			
			$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    		$db->beginTransaction();

			$stmt = $db->prepare('	SELECT 
										a . * , 
										b.OrderLine_ProductNameATOP, 
										b.OrderLine_QuantitySoldATOP, 
										b.OrderLine_AssignedToFactoryId, 
										b.OrderLine_Dispatched
									FROM 
										Orders a
									CROSS JOIN 
										OrderLines b 
										ON 
											a.Order_Id = b.Order_Id
									WHERE (
										a.Order_State =2 OR 
										a.Order_State =3
										)');
										
										
			$stmt->execute();
			$result1 = $stmt->fetchAll(PDO::FETCH_ASSOC);


			$stmt = $db->prepare('SELECT * FROM Factories');
			$stmt->execute();
			$result3 = $stmt->fetchAll(PDO::FETCH_ASSOC);

			$db->commit();
					
					


		
			
			$array = array(
				"orderResults"=>$result1,
				"factoriesResult"=>$result3		
			);
			sendResponse(200, json_encode($array));
			return;
		}
		catch(PDOException $e){
			$db->rollBack();
			sendResponse(400, '{"Error":"3002", "Message":"'. $e->getMessage() .'"}');
		}
	}
	
	function finaliseOrderForCEO(){
	
	
	
	
	
	
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	function retrieveAllOrdersForPhaseOne_FactoryPlanning(){
		try{
			$db = getConnection();
			
			$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    		/* Begin a transaction, turning off autocommit */
    		$db->beginTransaction();
					

			$stmt = $db->prepare('SELECT * FROM Orders WHERE Order_State = 1');
			$stmt->execute();
			$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
			
			$stmt = $db->prepare('SELECT a.Order_Id, a.Agent_Id, b.Product_Id, a.Order_CreatedDate, a.Order_ReferenceNumber, b.OrderLine_Id, b.OrderLine_ProductNameATOP, b.OrderLine_QuantitySoldATOP, b.OrderLine_AssignedToFactoryId
			FROM Orders a, OrderLines b
			WHERE a.Order_Id = b.Order_Id AND a.Order_State = 1');
			$stmt->execute();
			$result2 = $stmt->fetchAll(PDO::FETCH_ASSOC);
			
			
			
			$stmt = $db->prepare('SELECT * FROM Factories');
			$stmt->execute();
			$result3 = $stmt->fetchAll(PDO::FETCH_ASSOC);

			$db->commit();

		
			
			$array = array(
				"orderResults"=>$result,
				"detailedOrderResults"=>$result2,
				"factoriesResult"=>$result3		
		
			);
			sendResponse(200, json_encode($array));
			
		}
    	catch(PDOException $e){
			$db->rollBack();

			sendResponse(400, '{"Error":"3002", "Message":"'. $e->getMessage() .'"}');
		}
		return;	
	};
	
	
	
	function updateOrdersToStateTwo(){

		$result = getJSONDecodedPostData();
    	
    	if($result != NULL){
			if(array_key_exists('ordersobjects', $result)){
				
				/*
date_default_timezone_set('Asia/Calcutta');

				$script_tz = date_default_timezone_get();

				//echo " $result: " . $result; 
*/
				$set = $result->ordersobjects;
				//
				$array = $set->invoiceitems;
				//echo $object->agentId;
				
				$referenceNumber = $set->referenceNumber;
				$agentId = $set->agentId;
				
				try{
					$db = getConnection();
					$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    				/* Begin a transaction, turning off autocommit */
					$db->beginTransaction();
					
					$stmt = $db->prepare(' UPDATE Orders SET Order_State = 2 WHERE Order_ReferenceNumber = :orn AND Agent_Id = :aid');
					$stmt->bindParam(':orn', $referenceNumber);
					$stmt->bindParam(':aid', $agentId);
					$stmt->execute();
						
						for($i = 0; $i < count($array); $i++){
								$object = $array[$i];

			
							$stmt = $db->prepare('UPDATE OrderLines SET OrderLine_AssignedToFactoryId = :fid WHERE OrderLine_Id = :oid');
							$stmt->bindParam(':fid', $object->assignedToFactoryId);
							$stmt->bindParam(':oid', $object->orderLineId);
							$stmt->execute();
							
									

				
					//}		
											/*
							agentId = 2;
							assignedToFactoryId = 4;
							factoryName = "PY-KB";
							orderId = 10;
							productName = "1 kg - Rs 40";
							quantitySold = 25;
							referenceCode = "Test_Tamilnadu/1802/003";
						*/
						//echo '{"agentId":"' . $object->agentId . '", ';
						//echo '"orderlineid:"' . $object->orderLineId .'", ';
						//echo '"assignedToFactoryId":"' . $object->assignedToFactoryId . '"}';
						//echo '"factoryName":"' . $object->factoryName . '", ';
						//echo '"orderId":"' . $object->orderId . '", ';
						//echo '"productName":"' . $object->productName . '", ';
						//echo '"quantitySold":"' . $object->quantitySold . '", ';
						//echo '"referenceCode":"' . $object->referenceCode . '"}';
						//echo '                                                 ';
						}
						$db->commit();

						sendResponse(200, '{"Error":"0", "Message":"Factory planning arranged", "OrderReferenceNumber":"' . $referenceNumber . '"}');
						return;
					}
		    	catch(PDOException $e){
		    	/* Recognize mistake and roll back changes */
					$db->rollBack();
    				sendResponse(400, '{"Error":"3002", "Message":"'. $e->getMessage() .'"}');
    				return;
    			}


			}
			sendResponse(400, '{"Error":"3001", "Message":"Appropriate \'order\' keys not passed in"}');
			return;

			
		}
		sendResponse(400, '{"Error":"3000", "Message":"You must pass in \'order\' data to filter on"}');
		return;

	}





	function retrieveAllOrdersForPhaseTwo_FactoryViewing(){

		try{
			$db = getConnection();
			
			$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    		/* Begin a transaction, turning off autocommit */
    		$db->beginTransaction();
					

			$stmt = $db->prepare('SELECT * FROM Orders WHERE Order_State = 2');
			$stmt->execute();

			$result1 = $stmt->fetchAll(PDO::FETCH_ASSOC);
			
			$stmt = $db->prepare('SELECT a.Order_Id, a.Agent_Id, a.Order_CreatedDate, a.Order_ReferenceNumber, b.OrderLine_Id, b.OrderLine_ProductNameATOP, b.OrderLine_QuantitySoldATOP, b.OrderLine_AssignedToFactoryId, b.OrderLine_Dispatched
			FROM Orders a, OrderLines b
			WHERE a.Order_Id = b.Order_Id AND a.Order_State = 2
			AND OrderLine_Dispatched = 0');
			$stmt->bindParam(':fid', $result->factoryid);

			$stmt->execute();
			$result2 = $stmt->fetchAll(PDO::FETCH_ASSOC);
			
			
			
				
			$stmt = $db->prepare('SELECT * FROM Factories');
			$stmt->execute();
			$result3 = $stmt->fetchAll(PDO::FETCH_ASSOC);

			$db->commit();
					
			$array = array(
				"orderResults"=>$result1,
				"detailedOrderResults"=>$result2,
				"factoriesResult"=>$result3			
			);
			sendResponse(200, json_encode($array));
		
		}
		catch(PDOException $e){
			$db->rollBack();

			sendResponse(400, '{"Error":"3002", "Message":"'. $e->getMessage() .'"}');
		}
		return;	

	}
	

	function retrieveAllOrdersForPhaseTwo_FactoryPlanning(){
		$result = getJSONDecodedPostData();
    	
    	if($result != NULL){
			if(array_key_exists('factoryid', $result)){
				try{
					$db = getConnection();
					
					$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		    		/* Begin a transaction, turning off autocommit */
		    		$db->beginTransaction();
							
		
					$stmt = $db->prepare('SELECT * FROM Orders WHERE (Order_State = 2 || Order_State = 3) ');
					$stmt->execute();

					$result1 = $stmt->fetchAll(PDO::FETCH_ASSOC);
					
					$stmt = $db->prepare('SELECT a.Order_Id, a.Agent_Id, a.Order_CreatedDate, a.Order_State, a.Order_ReferenceNumber, b.OrderLine_Id, b.OrderLine_ProductNameATOP, b.OrderLine_QuantitySoldATOP, b.OrderLine_AssignedToFactoryId, b.OrderLine_Dispatched
					FROM Orders a, OrderLines b
					WHERE a.Order_Id = b.Order_Id AND (a.Order_State = 2 || a.Order_State = 3) AND b.OrderLine_AssignedToFactoryId = :fid');
					$stmt->bindParam(':fid', $result->factoryid);

					$stmt->execute();
					$result2 = $stmt->fetchAll(PDO::FETCH_ASSOC);
					
					
					
						
					$stmt = $db->prepare('SELECT * FROM Factories');
					$stmt->execute();
					$result3 = $stmt->fetchAll(PDO::FETCH_ASSOC);
		
					$db->commit();
							
					$array = array(
						"orderResults"=>$result1,
						"detailedOrderResults"=>$result2,
						"factoriesResult"=>$result3			
					);
					sendResponse(200, json_encode($array));
				
				}
	    		catch(PDOException $e){
					$db->rollBack();
	
					sendResponse(400, '{"Error":"3002", "Message":"'. $e->getMessage() .'"}');
				}
				return;	
			}
			sendResponse(400, '{"Error":"3001", "Message":"Appropriate \'order\' keys not passed in"}');
			return;
		}
		sendResponse(400, '{"Error":"3000", "Message":"You must pass in \'order\' data to filter on"}');
		return;
	}
	
	
	
	
	
	
	function updateOrdersWithStateTwoToDispatched(){

		$result = getJSONDecodedPostData();
    	
    	if($result != NULL){
			if(array_key_exists('ordersobjects', $result)){
				
				/*
date_default_timezone_set('Asia/Calcutta');

				$script_tz = date_default_timezone_get();

				//echo " $result: " . $result; 
*/
				$set = $result->ordersobjects;
				//
				$array = $set->invoiceitems;
				//echo $object->agentId;
				
				$referenceNumber = $set->referenceNumber;
				$agentId = $set->agentId;
				$orderId = $set->orderId;
				
				try{

					$db = getConnection();
					$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
					$db->beginTransaction();
					
					/*
$stmt = $db->prepare(' UPDATE Orders SET Order_State = 2 WHERE Order_ReferenceNumber = :orn AND Agent_Id = :aid');
					$stmt->bindParam(':orn', $referenceNumber);
					$stmt->bindParam(':aid', $agentId);
					$stmt->execute();
*/

						
						for($i = 0; $i < count($array); $i++){
								$object = $array[$i];

			
							$stmt = $db->prepare('UPDATE OrderLines SET OrderLine_Dispatched = 1 WHERE OrderLine_Id = :oid');
							$stmt->bindParam(':oid', $object->orderLineId);
							$stmt->execute();
							
									

				
					//}		
											/*
							agentId = 2;
							assignedToFactoryId = 4;
							factoryName = "PY-KB";
							orderId = 10;
							productName = "1 kg - Rs 40";
							quantitySold = 25;
							referenceCode = "Test_Tamilnadu/1802/003";
						*/
							//echo '{"agentId":"' . $object->agentId . '"}';
							/*
echo '"orderlineid:"' . $object->orderLineId . '", ';
							//echo '"assignedToFactoryId":"' . $object->assignedToFactoryId . '"}';
							//echo '"factoryName":"' . $object->factoryName . '", ';
							//echo '"orderId":"' . $object->orderId . '", ';
							//echo '"productName":"' . $object->productName . '", ';
							//echo '"quantitySold":"' . $object->quantitySold . '", ';
							echo '"referenceCode":"' . $object->referenceCode . '"}';
*/
							//echo '                                                 ';
						}
						
						
						$stmt = $db->prepare('	SELECT *FROM Orders a, OrderLines b
												WHERE a.Order_Id = b.Order_Id
												AND a.Order_State = 2
												AND a.Order_Id = :oid');
												
						$stmt->bindParam(':oid', $orderId);
						$stmt->execute();
						$result1 = $stmt->fetchAll(PDO::FETCH_ASSOC);
						
						$canOrderBeDispatched = TRUE;
						for($i = 0; $i < count($result1); $i++){
							if($result1[$i]['OrderLine_Dispatched'] == 0){
								$canOrderBeDispatched = FALSE;
								break;
							}
						}
						
						if($canOrderBeDispatched){
							//echo "Order CAN be dispatched ";
							$stmt = $db->prepare(' UPDATE Orders SET Order_State = 3 WHERE Order_ReferenceNumber = :orn AND Agent_Id = :aid');
							$stmt->bindParam(':orn', $referenceNumber);
							$stmt->bindParam(':aid', $agentId);
							$stmt->execute();
						}
						else{
							//echo "Order can't be dispatched because full order isn't complete";
						}
						
						//echo json_encode($result1);
						$db->commit();

						sendResponse(200, '{"Error":"0", "Message":"Order items dispatched", "OrderReferenceNumber":"' . $referenceNumber . '"}');
						return;
					}
		    	catch(PDOException $e){
		    	/* Recognize mistake and roll back changes */
					$db->rollBack();
    				sendResponse(400, '{"Error":"3002", "Message":"'. $e->getMessage() .'"}');
    				return;
    			}


			}
			sendResponse(400, '{"Error":"3001", "Message":"Appropriate \'order\' keys not passed in"}');
			return;

			
		}
		sendResponse(400, '{"Error":"3000", "Message":"You must pass in \'order\' data to filter on"}');
		return;

	}





	function updateOrdersSkipStraightToFinaliseScreen(){

		$result = getJSONDecodedPostData();
    	
    	if($result != NULL){
			if(array_key_exists('ordersobjects', $result)){
				
	
				$set = $result->ordersobjects;
				
				$referenceNumber = $set->referenceNumber;
				$agentId = $set->agentId;
				$orderId = $set->orderId;
				
				try{

					$db = getConnection();
					$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
					$db->beginTransaction();
						
					//echo "Order CAN be dispatched ";
					$stmt = $db->prepare(' UPDATE Orders SET Order_State = 3 WHERE Order_ReferenceNumber = :orn AND Agent_Id = :aid');
					$stmt->bindParam(':orn', $referenceNumber);
					$stmt->bindParam(':aid', $agentId);
					$stmt->execute();
					$db->commit();

					sendResponse(200, '{"Error":"0", "Message":"Order items dispatched", "OrderReferenceNumber":"' . $referenceNumber . '"}');
					return;
				}
		    	catch(PDOException $e){
		    	// Recognize mistake and roll back changes 
					$db->rollBack();
    				sendResponse(400, '{"Error":"3002", "Message":"'. $e->getMessage() .'"}');
    				return;
    			}


			}
			sendResponse(400, '{"Error":"3001", "Message":"Appropriate \'order\' keys not passed in"}');
			return;

			
		}
		sendResponse(400, '{"Error":"3000", "Message":"You must pass in \'order\' data to filter on"}');
		return;

	}

	
	
	

	function updateOrderReset(){
		$result = getJSONDecodedPostData();
    	
    	if($result != NULL){
			if(array_key_exists('orderid', $result)){				
				try{
					$db = getConnection();
					$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
					$db->beginTransaction();
						
					$stmt = $db->prepare(' UPDATE Orders SET Order_State = 1 WHERE Order_Id = :oid');
					$stmt->bindParam(':oid', $result->orderid);
					$stmt->execute();
					$db->commit();

					sendResponse(200, '{"Error":"0", "Message":"Order factory planning reset"}');
					return;
				}
		    	catch(PDOException $e){
		    	// Recognize mistake and roll back changes 
					$db->rollBack();
    				sendResponse(400, '{"Error":"3002", "Message":"'. $e->getMessage() .'"}');
    				return;
    			}


			}
			sendResponse(400, '{"Error":"3001", "Message":"Appropriate \'order\' keys not passed in"}');
			return;

			
		}
		sendResponse(400, '{"Error":"3000", "Message":"You must pass in \'order\' data to filter on"}');
		return;

	}





//this is where get state three orders for ceo finalise screen code would be
	
	
	
	
	
	
	
	
	
	
	
	













	
	
	
	
	
	
	
	
	
	
	
	
	
	

	function deleteOrderStateOne(){
		$result = getJSONDecodedPostData();
    	if($result != NULL){
    	
 			if(array_key_exists('orderobjecttodelete', $result)){
				$orderId = $result->orderobjecttodelete;
				
				$referenceNumber = $result->orderReferenceNumber;
				

				try{
					$db = getConnection();
					$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    				/* Begin a transaction, turning off autocommit */
					$db->beginTransaction();
					
	
					
					$stmt = $db->prepare('DELETE FROM Orders WHERE Order_Id = :oid');
					$stmt->bindParam(':oid', $orderId);
					$stmt->execute();
					
					$stmt = $db->prepare('DELETE FROM OrderLines WHERE Order_Id = :oid');
					$stmt->bindParam(':oid', $orderId);
					$stmt->execute();

					$db->commit();

					sendResponse(200, '{"Error":"0", "Message":"Order has been deleted", "OrderReferenceNumber":"' . $referenceNumber . '"}');
					return;
				}
		    	catch(PDOException $e){
		    	/* Recognize mistake and roll back changes */
					$db->rollBack();
    				sendResponse(400, '{"Error":"3002", "Message":"'. $e->getMessage() .'"}');
    				return;
    			}


/*
				 DELETE FROM table_name
WHERE some_column=some_value
*/

			}
			sendResponse(400, '{"Error":"3001", "Message":"Appropriate \'order\' keys not passed in"}');
			return;
		}
		sendResponse(400, '{"Error":"3000", "Message":"You must pass in \'order\' data to filter on"}');
		return;
	
	}
	
	
	
	
	
	
	
		function retrieveAllOrdersForEditOrders(){
		try{
			$db = getConnection();
			
			$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    		/* Begin a transaction, turning off autocommit */
    		$db->beginTransaction();
					

			$stmt = $db->prepare('SELECT a . * , b.State_Id 
									FROM Orders a, Agents b 
									WHERE 	a.Agent_Id = b.Agent_Id 
										AND a.Order_State =1');
										
			$stmt->execute();
			$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
			
			$stmt = $db->prepare('SELECT a.Order_Id, a.Agent_Id, b.Product_Id, a.Order_CreatedDate, a.Order_ReferenceNumber, b.OrderLine_Id, b.OrderLine_ProductNameATOP, b.OrderLine_QuantitySoldATOP, b.OrderLine_AssignedToFactoryId
			FROM Orders a, OrderLines b
			WHERE a.Order_Id = b.Order_Id AND a.Order_State = 1');
			$stmt->execute();
			$result2 = $stmt->fetchAll(PDO::FETCH_ASSOC);
			
			$db->commit();

			$array = array(
				"orderResults"=>$result,
				"detailedOrderResults"=>$result2,
			);
			sendResponse(200, json_encode($array));
			
		}
    	catch(PDOException $e){
			$db->rollBack();

			sendResponse(400, '{"Error":"3002", "Message":"'. $e->getMessage() .'"}');
		}
		return;	
	};
	
	
	
	
	
	function updateExistingOrder(){
	
		$result = getJSONDecodedPostData();

		//echo json_encode($result);	
		$object = $result->orderobject;
		$invoiceItems = $object->invoiceitems;
		
		$referenceNum = $object->referenceNumber;
		
		//echo "referenceNumber: " . $referenceNum;
		
		$agentId = $object->agentid;
			
		try{
			$db = getConnection();
			
			$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    		/* Begin a transaction, turning off autocommit */
    		$db->beginTransaction();
					
			$stmt = $db->prepare('SELECT * FROM Orders WHERE Order_ReferenceNumber = :ref');
			$stmt->bindParam(':ref', $referenceNum);

										
			$stmt->execute();
			$orderSetResult = $stmt->fetchAll(PDO::FETCH_ASSOC);
	
	
/*  			echo "             array: " . JSON_ENCODE($orderSetResult);  */

			$orderId = $orderSetResult[0]['Order_Id'];
			
			
/* 			echo "number: " . $orderId; */
			
			
			$stmt = $db->prepare('SELECT * FROM OrderLines WHERE Order_Id = :oid');
			$stmt->bindParam(':oid', $orderId);

										
			$stmt->execute();
			$orderLineSetResult = $stmt->fetchAll(PDO::FETCH_ASSOC);

			
			/*
			echo "
			
			
			
			invoiceItems:
			" . JSON_ENCODE($invoiceItems);
			
			echo "
			
			
			
			orderLineSetResult:
			" . JSON_ENCODE($orderLineSetResult);
			*/
			 

			
 			for($k = 0; $k < count($invoiceItems); $k++){
 				 $matched = FALSE;

 				for($i = 0; $i < count($orderLineSetResult); $i++){
 					$currentInvoicePId = $invoiceItems[$k]->productid;
 					
 					$currentOLPId = $orderLineSetResult[$i]['Product_Id'];
 					
 					
 					
 					//If the product exists and is in the order lines result then check to see if the quantity is the same.
 					//if not then update that specific orderline
 					if($currentInvoicePId == $currentOLPId){
// 					 					echo "
// 					result pid: " . $currentInvoicePId . " olresultpid: " . $currentOLPId; 

 						$currentInvoiceQuantitySold = $invoiceItems[$k]->quantitysold;
 						$currentOLQuantitySold = $orderLineSetResult[$i]['OrderLine_QuantitySoldATOP'];
 						
 						
// 						echo "
// 						invoice quantity sold: " . $currentInvoiceQuantitySold . " olquantitysold: " . $currentOLQuantitySold; 

 						if($currentInvoiceQuantitySold != $currentOLQuantitySold){
 							
 							$stmt = $db->prepare('UPDATE OrderLines SET OrderLine_QuantitySoldATOP = :qs WHERE OrderLine_Id = :olid');
							$stmt->bindParam(':qs', $currentInvoiceQuantitySold);
							$stmt->bindParam(':olid', $orderLineSetResult[$i]['OrderLine_Id']);
							$stmt->execute();
 						}
 						$matched = TRUE;
 					}
				}
				if($matched == FALSE){
				//Get pbox discounts if any before inserting one new OrderLine				
					$stmt = $db->prepare('SELECT * FROM PBoxDiscounts WHERE Agent_Id = :aid');
					$stmt->bindParam(':aid', $agentId);
					$stmt->execute();				
					$PBoxDiscountsArray = $stmt->fetchAll(PDO::FETCH_ASSOC);
					
					
					//Product Not found so add a new row in the orderline using that particular order id
					$stmt = $db->prepare('INSERT INTO OrderLines VALUES(\'\', :oid, :aod, :pid, :pn, :pp, :pbd, :qs, :qpb, \'\', \'\')');
					$stmt->bindParam(':oid', $orderId);
					$stmt->bindParam(':aod', $agentId);
					
					
					$currentBoxDiscount = 0;
	
					//check for all products the customer has box discounts for if any
					for($j = 0; $j < count($PBoxDiscountsArray); $j++){
						//if the agent does have a box discount.. check the product id and grab the one that matches the product id in the current invoice item
						if($PBoxDiscountsArray[$j]['Product_Id'] == $invoiceItems[$k]->productid){
	/* 	    						echo "ahahahaha              PBoxDiscountsArray[j]['Product_Id'] = " . $PBoxDiscountsArray[$j]['Product_Id'] .  " and $invoiceItems[$i]->productid = "; */
							$currentBoxDiscount = $PBoxDiscountsArray[$j]['PBoxDiscount_RupeesOffForBox'];
							break;
						}
					}
					$stmt->bindParam(':pid', $invoiceItems[$k]->productid);
					$stmt->bindParam(':pn', $invoiceItems[$k]->productname);
					$stmt->bindParam(':pp', $invoiceItems[$k]->productprice);
					$stmt->bindParam(':pbd', $currentBoxDiscount);
	
					$stmt->bindParam(':qs', $invoiceItems[$k]->quantitysold);
					$stmt->bindParam(':qpb', $invoiceItems[$k]->quantityperbox);
					$stmt->execute();
				}
			}
			
			//echo "    count invoice items: " . count($invoiceItems); 				
				
			
			
			
			
			
			
			
			
			
			//Delete any rows
			
			for($k = 0; $k < count($orderLineSetResult); $k++){
 				 $matched = FALSE;

 				for($i = 0; $i < count($invoiceItems); $i++){
 					$currentInvoicePId = $invoiceItems[$i]->productid;
 					
 					$currentOLPId = $orderLineSetResult[$k]['Product_Id'];
 					
 					
 					if($currentOLPId == $currentInvoicePId){
 					 					//echo "
 										//	result pid: " . $currentInvoicePId . " olresultpid: " . $currentOLPId; 
 						break;
					}
					else{
					
						if($i == count($invoiceItems)-1){
/*
							echo "
								DELETE: " . $currentOLPId;
*/
							$stmt = $db->prepare('DELETE FROM OrderLines WHERE Order_Id = :oid AND Product_Id = :pid');
							$stmt->bindParam(':oid', $orderId);
							$stmt->bindParam(':pid', $currentOLPId);

							$stmt->execute();

						}
					
					}
				}
				
			}
					
			
			
			
			
			
			
			
			
			
			$db->commit();
/*

			$array = array(
				"orderResults"=>$result,
				"detailedOrderResults"=>$result2,
			);
			sendResponse(200, json_encode($array));
*/
			sendResponse(200, '{"Error":"0", "Message":"Order has been updated", "OrderReferenceNumber":"' . $referenceNum . '"}');

			
		}
    	catch(PDOException $e){
			$db->rollBack();

			sendResponse(400, '{"Error":"3002", "Message":"'. $e->getMessage() .'"}');
		}
		return;	

	}
	
	
	
	
	
	
	
	function retrieveFactoryDonePage(){
		$result = getJSONDecodedPostData();
    	if($result != NULL){
    	
 			if(	array_key_exists('factoryid', $result) &&
 				array_key_exists('dispatched', $result)){
				$factoryId = $result->factoryid;
				
				

				try{
					$db = getConnection();
					
					$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		    		$db->beginTransaction();
	
					$stmt = $db->prepare('	SELECT 
												a.Order_Id, 
												a.Order_State, 
												a.Order_ReferenceNumber, 
												b.OrderLine_ProductNameATOP, 
												b.OrderLine_QuantitySoldATOP, 
												b.OrderLine_AssignedToFactoryId, 
												b.OrderLine_Dispatched
												
											FROM   Orders a 
										       CROSS JOIN OrderLines b 
		       										ON a.Order_Id = b.Order_Id
		       
		      											WHERE (a.Order_State = 2 OR a.Order_State = 3)
		      											AND
													       b.OrderLine_AssignedToFactoryId = :fid
		       
		       											AND 
													 	b.OrderLine_Dispatched = :dbool');
					$stmt->bindParam(':fid', $factoryId);
					//1 is dispatched 0 is not // dispatched bool is what ive saved it as
					$stmt->bindParam(':dbool', $result->dispatched);
					$stmt->execute();
					$result1 = $stmt->fetchAll(PDO::FETCH_ASSOC);
		
					$db->commit();
							
					$array = array(
						"orderResults"=>$result1,
					);
					sendResponse(200, json_encode($array));
					return;
				}
				catch(PDOException $e){
					$db->rollBack();
		
					sendResponse(400, '{"Error":"3002", "Message":"'. $e->getMessage() .'"}');
					return;
				}
			}
			sendResponse(400, '{"Error":"3001", "Message":"Appropriate \'order\' keys not passed in"}');
			return;
		}
		sendResponse(400, '{"Error":"3000", "Message":"You must pass in \'order\' data to filter on"}');
		return;
	}
	

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	function updateOrdersToStateFour(){

		$result = getJSONDecodedPostData();
    	
    	if($result != NULL){
			if(array_key_exists('ordersobjects', $result)){
				
				/*
date_default_timezone_set('Asia/Calcutta');

				$script_tz = date_default_timezone_get();

				//echo " $result: " . $result; 
*/
				$set = $result->ordersobjects;
				
				$agentId = $set->agentId;
				$referenceNumber = $set->referenceNumber;


				//these variables are set at the end once everything is calculated
				$finalTotalOrderPrice = 0;
				$finalTotalBoxDiscount = 0;
				
				try{
					$db = getConnection();
					$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    				/* Begin a transaction, turning off autocommit */
					$db->beginTransaction();
					
					
					
					
					
					
/* 					echo "HELLO";  */
					
					
					//#################
					//###   STEP ONE -- GET ALL BOX DISCOUNTS FOR AGENT WHOS ORDER IS BEING FINALISED
					//#################
					
					$stmt = $db->prepare('SELECT * FROM PBoxDiscounts WHERE Agent_Id = :aid');
    				$stmt->bindParam(':aid', $agentId);
    				$stmt->execute();
    				$PBoxDiscountsArray = $stmt->fetchAll(PDO::FETCH_ASSOC);
					

					//#################
					//###   STEP TWO --
					//### 				CALCULATE TOTAL ORDER PRICE 
					//###				AND CALCULATE TOTALBOX PER DISCOUNT
					//#################
					
					$stmt = $db->prepare('
SELECT a.Order_Id, a.Agent_Id, a.Order_CreatedDate, a.Order_ReferenceNumber, a.Order_State, b . *
FROM Orders a, OrderLines b
WHERE a.Order_Id = b.Order_Id
AND a.Order_ReferenceNumber = :ref');

					$stmt->bindParam(':ref', $referenceNumber);
					$stmt->execute();

					
					$orderLinesResult = $stmt->fetchAll(PDO::FETCH_ASSOC);
						
						
					//#################
					//###  STEP THREE -- ITERATE THROUGH ALL THE ORDER LINES RELATED TO THE REFERENCE NUMBER TO CALCULATE THE TOTAL PRICE
					//#################
					
					//TEMP VARIABLES
					$totalPriceOfOrder = 0;
					$totalBoxDiscount = 0;

					for($i = 0; $i < count($orderLinesResult); $i++){
					
						$totalPriceOfOrder += $orderLinesResult[$i]['OrderLine_PriceOfProductATOP'] * $orderLinesResult[$i]['OrderLine_QuantitySoldATOP'];
						$currentBoxDiscount = 0;
	    				for($j = 0; $j < count($PBoxDiscountsArray); $j++){
	    					//if the agent does have a box discount.. check the product id and grab the one that matches the product id in the current invoice item
	    					if($PBoxDiscountsArray[$j]['Product_Id'] == $orderLinesResult[$i]['Product_Id']){
	    						$currentBoxDiscount = $PBoxDiscountsArray[$j]['PBoxDiscount_RupeesOffForBox'];
	    						$totalBoxDiscount += $currentBoxDiscount * $orderLinesResult[$i]['OrderLine_QuantitySoldATOP'];
	    						break;
	    					}
	    				}
					}
					
					
					//#################
					//###  STEP FOUR -- GET PERCENTAGE DISCOUNT FOR AGENT WHOS ORDER THATS BEING FINALISED
					//#################
					
					$stmt = $db->prepare('SELECT * FROM Agents WHERE Agent_Id = :aid');
					$stmt->bindParam(':aid', $agentId);
					$stmt->execute();
					$result2 = $stmt->fetchAll(PDO::FETCH_ASSOC);
					
					$agentPercentageDiscount = (min(max(intval($result2[0]['Agent_PercentageDiscount']), 0), 99));

					

			/*
		echo "
					
					agent percentage discount: " . $agentPercentageDiscount;
					
*/


					
					

					if($agentPercentageDiscount == 0){
						$agentPercentageDiscount = 0;
					}

		
/*

 					//$ = 






					echo "
					
					total price: " . $totalPriceOfOrder;
					
					echo "
					
					total box discount: " . $totalBoxDiscount;
										
					
					
					

					
					
*/
					$totalPercentageDiscount = round((($totalPriceOfOrder-$totalBoxDiscount) * $agentPercentageDiscount)/100);
					/*
echo "
					
					agent final percentage discount 2: " . $totalPercentageDiscount;
*/

					$orderLevelBalance = ($totalPriceOfOrder-$totalBoxDiscount) - $totalPercentageDiscount; 



					$finalTotalBoxDiscount = $totalBoxDiscount;
					$finalPercentageDiscount = $totalPercentageDiscount;				
					$finalOrderLevelBalance = $orderLevelBalance;	
					$finalTotalOrderPrice = $totalPriceOfOrder;
				/*
	
							
			  		echo "
					
					order level balance	: " . $finalOrderLevelBalance;

	
	
*/
					
					$stmt = $db->prepare('	UPDATE Orders 
												SET 
													Order_TotalBoxDiscount = :bd,
													Order_TotalPercentageDiscount = :pd,
													Order_LevelBalance = :lb,
													Order_TotalAmount = :ta,
													Order_FinalisedDate = :fd
												WHERE Order_ReferenceNumber = :orn AND Agent_Id = :aid');
												
												
					$stmt->bindParam(':bd', $finalTotalBoxDiscount);
					$stmt->bindParam(':pd', $finalPercentageDiscount);
					$stmt->bindParam(':lb', $finalOrderLevelBalance);
					$stmt->bindParam(':ta', $finalOrderLevelBalance);//$finalTotalOrderPrice);
					$stmt->bindParam(':orn', $referenceNumber);
					
					
					
					
					date_default_timezone_set('Asia/Calcutta');
					$script_tz = date_default_timezone_get();
					$date = date('Y-m-d H:i:s');

					$stmt->bindParam(':fd', $date);
					$stmt->bindParam(':aid', $agentId);

					
/* 					echo "agent id ". $referenceNumber; */
					$stmt->execute();

					

					//CEO finalises order
					/*
					$stmt = $db->prepare('UPDATE Orders SET Order_State = 4 WHERE Order_ReferenceNumber = :orn AND Agent_Id = :aid');
					$stmt->bindParam(':orn', $referenceNumber);
					$stmt->bindParam(':aid', $agentId);
					$stmt->execute();
*/


					//Lets see if the agent has some credit in his account that is sufficient for the order price. If so then we can close the order straight away by updating the order state to 5.
					
					
					
					/*
					$stmt = $db->prepare('SELECT a.Order_Id, a.Agent_Id, a.Order_CreatedDate, a.Order_ReferenceNumber, a.Order_State, b.*
											FROM Orders a, OrderLines b
											WHERE 	a.Order_Id = b.Order_Id 
													AND a.Order_State = 3 
													AND a.Order_ReferenceNumber = :orn');

					$stmt->bindParam(':orn', $referenceNumber);
					$stmt->execute();

					
					$result1 = $stmt->fetchAll(PDO::FETCH_ASSOC);
						
					$totalPriceOfOrder = 0;
					for($i = 0; $i < count($result1); $i++){
					
						$totalPriceOfOrder += $result1[$i]['OrderLine_PriceOfProductATOP'] * $result1[$i]['OrderLine_QuantitySoldATOP'];
						
					}
*/
					
					$stmt = $db->prepare('SELECT * FROM Agents WHERE Agent_Id = :aid');
					$stmt->bindParam(':aid', $agentId);
					$stmt->execute();
					$result2 = $stmt->fetchAll(PDO::FETCH_ASSOC);
					
					$agentBalance = $result2[0]['Agent_Balance'];
					
					
					
					$stmt = $db->prepare('SELECT * FROM Orders WHERE Order_ReferenceNumber = :orn AND Agent_Id = :aid AND Order_State = 3');
					$stmt->bindParam(':orn', $referenceNumber);
					$stmt->bindParam(':aid', $agentId);
					$stmt->execute();
					
					$result3 = $stmt->fetchAll(PDO::FETCH_ASSOC);
				
					$orderLevelBalance = $result3[0]['Order_LevelBalance'];
					/*
echo "
					
					FINALISING Order level balance: " . $orderLevelBalance . "."; 
*/
					
					/*
					echo '{"TotalPriceToPay":"'.$orderLevelBalance.'"}';
					echo '{"AgentBalance":"'.$agentBalance.'"}';
					*/
					
					//$totalOrderPrice = $totalPriceOfOrder; //16,540
					//$currentAgentBalance = $agentBalance;  //1,00,000
					
					
					/////////////////////////Check if the account is fully positive
					/////THE CUSTOMER CAN FULLY AFFORD TO PAY FOR THE ORDER
					//
					if($agentBalance > $orderLevelBalance){ //THE CUSTOMER CAN FULLY AFFORD TO PAY FOR THE ORDER
						//Update order will get closed by setting it to state 5;
						
	
						$stmt = $db->prepare('UPDATE Orders SET Order_State = 5, Order_LevelBalance = 0 WHERE Order_ReferenceNumber = :orn AND Agent_Id = :aid  AND Order_State = 3');
						$stmt->bindParam(':orn', $referenceNumber);
						$stmt->bindParam(':aid', $agentId);
						$stmt->execute();

						$finalBalance = $agentBalance-$orderLevelBalance;
						$stmt = $db->prepare('UPDATE Agents SET Agent_Balance = :ab WHERE Agent_Id = :aid');
						$stmt->bindParam(':ab', $finalBalance);
						$stmt->bindParam(':aid', $agentId);
						$stmt->execute();



						/*
$stmt = $db->prepare('SELECT * FROM Agents WHERE Agent_Id = :aid');
						$stmt->bindParam(':aid', $agentId);
						$stmt->execute();
						$result2 = $stmt->fetchAll(PDO::FETCH_ASSOC);
						
						$agentBalance = $result2[0]['Agent_Balance'];
						echo "{FINAL BALANCE IS " . $agentBalance . " :D}";
*/
					}
					//Will get executed when the order is greater than the agent balance yet the agent balance still being over 
					else if($agentBalance > 0){
						$newOrderLevelBalanceAfterPayingWhateverTheAgentCould = $orderLevelBalance-$agentBalance;
						
						$stmt = $db->prepare('UPDATE Orders SET Order_State = 4, Order_LevelBalance = :olb WHERE Order_ReferenceNumber = :orn AND Agent_Id = :aid  AND Order_State = 3');													$stmt->bindParam(':olb', $newOrderLevelBalanceAfterPayingWhateverTheAgentCould);
						$stmt->bindParam(':orn', $referenceNumber);
						$stmt->bindParam(':aid', $agentId);
						$stmt->execute();
						
						
						$finalBalance = $agentBalance-$orderLevelBalance;

						$stmt = $db->prepare('UPDATE Agents SET Agent_Balance = :ab WHERE Agent_Id = :aid');
						$stmt->bindParam(':ab', $finalBalance);
						$stmt->bindParam(':aid', $agentId);
						$stmt->execute();
					
					
					}
					else{ 
						// agent balance is less than or equal to 0.. so deduct agent balance only.. instead of updating order level balance.. since no payments could be adjusted due to no credit.
						$finalBalance = $agentBalance-$orderLevelBalance;
/*
						$stmt = $db->prepare('UPDATE Orders SET Order_State = 4, Order_LevelBalance = :olb WHERE Order_ReferenceNumber = :orn AND Agent_Id = :aid  AND Order_State = 3');													$stmt->bindParam(':olb', $newOrderLevelBalanceAfterPayingWhateverTheAgentCould);
						$stmt->bindParam(':orn', $referenceNumber);
						$stmt->bindParam(':aid', $agentId);
						$stmt->execute();
*/
						$stmt = $db->prepare('UPDATE Orders SET Order_State = 4  WHERE Order_ReferenceNumber = :orn AND Agent_Id = :aid  AND Order_State = 3');													
						$stmt->bindParam(':orn', $referenceNumber);
						$stmt->bindParam(':aid', $agentId);
						$stmt->execute();

						$stmt = $db->prepare('UPDATE Agents SET Agent_Balance = :ab WHERE Agent_Id = :aid');
						$stmt->bindParam(':ab', $finalBalance);
						$stmt->bindParam(':aid', $agentId);
						$stmt->execute();
					}

					//WORKS
					
//					Next step is to close the order if the agent has enough balance to pay for the order that the cep just closed 
										
					//if the payment cannot be made there and then then the order state remains the same.
					
					//in both instances the order will be debited from the agent's balance 
					
					$db->commit();

					sendResponse(200, '{"Error":"0", "Message":"Order has been finalised", "OrderReferenceNumber":"' . $referenceNumber . '"}');
					return;
				}
		    	catch(PDOException $e){
		    	/* Recognize mistake and roll back changes */
					$db->rollBack();
    				sendResponse(400, '{"Error":"3002", "Message":"'. $e->getMessage() .'"}');
    				return;
    			}


			}
			sendResponse(400, '{"Error":"3001", "Message":"Appropriate \'order\' keys not passed in"}');
			return;
		}
		sendResponse(400, '{"Error":"3000", "Message":"You must pass in \'order\' data to filter on"}');
		return;
	}


	
	
?>
