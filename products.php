<?php



/*
////////////////////////////////////////////////////////////////////
////////////// BEGINNING PRODUCT
*/
	
	function addProduct(){
	    $result = getJSONDecodedPostData();
    	
    	if($result != NULL){
			if(array_key_exists('productname', $result) && 
			   array_key_exists('productquantityperbox', $result) && 
			   array_key_exists('productweight', $result)) {
				
				try{
    				$db = getConnection();
    				$stmt = $db->prepare('INSERT INTO Products VALUES(\'\', :pn, :pqpb, :pw)');
    				$stmt->bindParam(':pn', $result->productname);
        			$stmt->bindParam(':pqpb', $result->productquantityperbox);
        			$stmt->bindParam(':pw', $result->productweight);
    				$stmt->execute();
    				sendResponse(200, '{"Error":"0", "Message":"Successfully created \'product\'"}');
    			}
		    	catch(PDOException $e){
    				sendResponse(400,
    				 '{"Error":"3002", "Message":"'. $e->getMessage() .'"}');
    			}
				return;	
			}
			sendResponse(400, '{"Error":"3001", "Message":"Appropriate \'product\' keys not passed in"}');
	   		return;
    	}
    	sendResponse(400, '{"Error":"3000", "Message":"You must pass in \'product\' data to filter on"}');
    	return;
   	}
   	
   	
   	   	
/*
   	
   	function deleteProducts(){
	    $result = getJSONDecodedPostData();
    	
    	if($result != NULL){
			if(array_key_exists('statestodelete', $result)) {
				$array = $result->statestodelete;
				$arrayOfRowsThatCouldntBeDeleted = array();
			
				$db = getConnection();

				for($i = 0; $i < count($array); $i++){
					
					try{
						// DELETE FROM `Rajam`.`States` WHERE `States`.`State_ID` =18 
		    			$stmt = $db->prepare('DELETE FROM States WHERE State_ID = :sid');
		    			$stmt->bindParam(':sid', $array[$i]);
		    			$stmt->execute();
		   				//sendResponse(200, '{"Error":"0", "Message":"Successfully deleted \'state\'"}');
					}
					catch(PDOException $e){
						array_push($arrayOfRowsThatCouldntBeDeleted, $array[$i]);

	    				//sendResponse(400, '{"Error":"3002", "Message":"'. $e->getMessage() .'"}');
	    			}
	    			
					//echo $array[$i] ." ";
					//return;
				}
				$field_data = array();
				for($i = 0; $i < count($arrayOfRowsThatCouldntBeDeleted); $i++){
					$field_data[$i] = $arrayOfRowsThatCouldntBeDeleted[$i];
				}
				
				//print $array;
				//echo json_encode(array_keys($arrayOfRowsThatCouldntBeDeleted))	;
				echo sendResponse(200, json_encode($field_data));
				return;

			}
			sendResponse(400, '{"Error":"3001", "Message":"Appropriate \'state\' keys not passed in"}');
	   		return;
    	}
    	sendResponse(400, '{"Error":"3000", "Message":"You must pass in \'state\' data to filter on"}');
    	return;
   	}
	
   	
*/
   	
	/*

	function editState(){
		$result = getJSONDecodedPostData();
		//This makes sure at least one of the key exists before intersecting the array with the master array
		//no point intersecting with the master array if one of the below keys don't even exist
		if($result != NULL){
			if(array_key_exists('id', $result) &&
			(array_key_exists('statename', $result) ||
			array_key_exists('abbreviation', $result))) {
				
				try{
					$db = getConnection();	
					$array1 = get_object_vars($result);
    				$array2 = array('statename'=>'','abbreviation'=>'');
    				
    				//$newArray;
    				$intersectionResult = array_intersect_key($array1, $array2);
    				
    				//factoryname
    				if(array_key_exists('statename', $result)){
    				
						$newArray["State_Name"] = $intersectionResult['statename'];
    				}
    				
       				//stateid
					if(array_key_exists('abbreviation', $result)){
    					$newArray['State_AbbreviatedName'] = $intersectionResult['abbreviation'];
    				}
    				
   					$sql = "UPDATE States SET ";
					foreach($newArray as $key => $value)
    				$sql .= "$key = '$value',";
					$sql = substr($sql,0,-1);       //Remove the last comma
    				
    				$stmt = $db->prepare($sql . ' WHERE State_Id = :id');
    				$stmt->bindParam(':id', $result->id);
    				
    				$stmt->execute();
	    			sendResponse(200, '{"Error":"0", "Message":"Successfully edited \'state\'"}');
    			}
		    	catch(PDOException $e){
    				sendResponse(400, '{"Error":"3002", "Message":"'. $e->getMessage() .'"}');
    				return;
    			}
				return;	
			}
			sendResponse(400, '{"Error":"3001", "Message":"Appropriate \'state\' keys not passed in"}');
	   		return;
    	}
    	sendResponse(400, '{"Error":"3000", "Message":"You must pass in \'state\' data to filter on"}');
    	return;
	}
*/

	function retrieveAllProducts(){
		try{
			$db = getConnection();
			$stmt = $db->prepare('SELECT * FROM Products');
			$stmt->execute();
			$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
			sendResponse(200, json_encode($result));
		}
    	catch(PDOException $e){
			sendResponse(400, '{"Error":"3002", "Message":"'. $e->getMessage() .'"}');
		}
		return;	
	};	
	
		//{"productid":"", "stateid":"", "price":""}
	function updatePrice(){
		$result = getJSONDecodedPostData();
		//This makes sure at least one of the key exists before intersecting the array with the master array
		//no point intersecting with the master array if one of the below keys don't even exist
		if($result != NULL){
			if(array_key_exists('productid', $result) &&
			(array_key_exists('stateid', $result)) &&
			(array_key_exists('productprice', $result))) {
				//
				try{
					$db = getConnection();	
					$stmt = $db->prepare('INSERT INTO StateProducts (StateProduct_Id, State_Id, Product_Id, StateProduct_Price)
	    				VALUES(NULL, :sid, :pid, :spp)
	    				ON DUPLICATE KEY UPDATE 
	    				StateProduct_Price = :spp');
					$stmt->bindParam(':sid', $result->stateid);
					$stmt->bindParam(':pid', $result->productid);
					$stmt->bindParam(':spp', $result->productprice);
    				$stmt->execute();

	    			sendResponse(200, '{"Error":"0", "Message":"Successfully updated Product Price"}');
    			}
		    	catch(PDOException $e){
/* 		    		echo "firstname: " . $result->firstname; */
    				sendResponse(400, '{"Error":"3002", "Message":"'. $e->getMessage() .'"}');
    				return;
    			}
				return;	
			}
			sendResponse(400, '{"Error":"3001", "Message":"Appropriate product price keys not passed in "' . json_encode($result). '"}');
	   		return;
    	}
    	sendResponse(400, '{"Error":"3000", "Message":"You must pass in product price data to filter on"}');
    	return;
    }

	function retrieveAllProductsPrices(){
		try{
			$db = getConnection();
			$stmt = $db->prepare('SELECT * FROM Products');
			$stmt->execute();
			$result2 = $stmt->fetchAll(PDO::FETCH_ASSOC);
			
			$stmt = $db->prepare('SELECT a.state_id, a.state_name, a.state_abbreviatedname, b.product_id, b.product_name, b.product_quantityperbox, b.product_weight, c.stateproduct_price 
FROM States a CROSS JOIN Products b LEFT OUTER JOIN StateProducts c 
ON a.state_id = c.state_id AND b.product_id = c.product_id');
			$stmt->execute();
			$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
			
			
			
			sendResponse(200, json_encode($result));
		}
    	catch(PDOException $e){
			sendResponse(400, '{"Error":"3002", "Message":"'. $e->getMessage() .'"}');
		}
		return;	
	}
	
	
	
	
	function retrieveBoxDiscountForAgent(){
	$result = getJSONDecodedPostData();
		//This makes sure at least one of the key exists before intersecting the array with the master array
		//no point intersecting with the master array if one of the below keys don't even exist
		if($result != NULL){
			if(array_key_exists('agentid', $result)) {
				//
				try{
					$db = getConnection();	
					$stmt = $db->prepare('	SELECT a .Agent_Id, a.Agent_Name, b .Product_Id, b.Product_Name, c.PBoxDiscount_RupeesOffForBox
											FROM Agents a CROSS JOIN Products b LEFT OUTER JOIN PBoxDiscounts c
											ON a.Agent_Id = c.Agent_Id AND b.Product_Id = c.Product_Id
											WHERE a.Agent_Id = :agentid');
											
					$stmt->bindParam(':agentid', $result->agentid);
    				$stmt->execute();
					$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
			
			
			
					sendResponse(200, json_encode($result));
		
	    			//sendResponse(200, '{"Error":"0", "Message":"Successfully updated box per discounts"}');
    			}
		    	catch(PDOException $e){
/* 		    		echo "firstname: " . $result->firstname; */
    				sendResponse(400, '{"Error":"3002", "Message":"'. $e->getMessage() .'"}');
    				return;
    			}
				return;	
			}
			sendResponse(400, '{"Error":"3001", "Message":"Appropriate product per discount keys not passed in "' . json_encode($result). '"}');
	   		return;
    	}
    	sendResponse(400, '{"Error":"3000", "Message":"You must pass in product per discount data to filter on"}');
    	return;	
	}
	
	
	
	function updateProductBoxDiscounts(){
		$result = getJSONDecodedPostData();
		//echo JSON_ENCODE($result);
		
		
		if($result != NULL){
			if(array_key_exists('objectstoupdate', $result)) {
				$arrayItems = $result->objectstoupdate;
	

				try{
					$db = getConnection();	
					
					for($i = 0; $i < count($arrayItems); $i++){
					 	$currentItem = $arrayItems[$i];
					 	
						//{"ProductId":1,"AgentId":108,"RupeesOff":0},
						$stmt = $db->prepare('SELECT * FROM PBoxDiscounts WHERE Agent_Id = :aid AND Product_Id = :pid');
						$stmt->bindParam(':aid', $currentItem ->AgentId);
						$stmt->bindParam(':pid', $currentItem->ProductId);
	    				$stmt->execute();
	    				
	    				
						//in the case where there is no record of the chosen set of product id and agent id
						if($stmt->fetch(PDO::FETCH_NUM) == 0){
							//$result1 = $stmt->fetchAll(PDO::FETCH_ASSOC);
							//as long as the rupees off is not 0 lets create a new set since there is a new pboxdiscount set to be added
 							if($currentItem->RupeesOff != 0){ 
								//echo "rupees off: " .  $currentItem->RupeesOff . " pid: " . $currentItem->ProductId . "
								//";    				$stmt = $db->prepare('INSERT INTO Products VALUES(\'\', :pn, :pqpb, :pw)');

								$stmt = $db->prepare('INSERT INTO PBoxDiscounts VALUES(\'\', :aid, :pid, :ro)');
								$stmt->bindParam(':aid', $currentItem ->AgentId);
								$stmt->bindParam(':pid', $currentItem->ProductId);
								$stmt->bindParam(':ro', $currentItem->RupeesOff);
	    						$stmt->execute();
							}
						}
						else {
								$stmt = $db->prepare('UPDATE PBoxDiscounts SET PBoxDiscount_RupeesOffForBox = :ro WHERE Agent_Id = :aid AND Product_Id = :pid');
								$stmt->bindParam(':aid', $currentItem ->AgentId);
								$stmt->bindParam(':pid', $currentItem->ProductId);
								$stmt->bindParam(':ro', $currentItem->RupeesOff);
	    						$stmt->execute();
						}
/*

						echo "
						
						" . json_encode($result1);
*/
					}
/*
					$stmt = $db->prepare('	SELECT * FROM PBoxDiscounts WHERE Agent_Id = :aid AND Product_Id = :pid');
											
											
											
					$stmt->bindParam(':aid', $result->agentid);
					$stmt->bindParam(':aid', $result->agentid);
    				$stmt->execute();
    				
//    				if($sth->fetch(PDO::FETCH_NUM) > 0)
					$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
			
			
			
					sendResponse(200, json_encode($result));
*/
		
	    			sendResponse(200, '{"Error":"0", "Message":"Discounts updated"}');
    			}
		    	catch(PDOException $e){
    				sendResponse(400, '{"Error":"3002", "Message":"'. $e->getMessage() .'"}');
    				return;
    			}
				return;	

			
			}
			sendResponse(400, '{"Error":"3001", "Message":"Appropriate box per discount keys not passed in "' . json_encode($result). '"}');
	   		return;

			
		}
		sendResponse(400, '{"Error":"3000", "Message":"You must pass in box per discount data to filter on"}');
    	return;	
	}
	
	function updateProductBoxDiscounts2(){
		$result = getJSONDecodedPostData();
		echo JSON_ENCODE($result);
		
		
		if($result != NULL){
			if(array_key_exists('objectstoupdate', $result)) {
							
			}
			sendResponse(400, '{"Error":"3001", "Message":"Appropriate box per discount keys not passed in "' . json_encode($result). '"}');
	   		return;

		}
		sendResponse(400, '{"Error":"3000", "Message":"You must pass in box per discount data to filter on"}');
    	return;	


	}
	
	
////////////// END PRODUCT
////////////////////////////////////////////////////////////////////

?>