<?php

////////////////////////////////////////////////////////////////////
////////////// BEGINNING AGENTS

	function addAgent(){
	    $result = getJSONDecodedPostData();
    	
    	if($result != NULL){
			if(array_key_exists('name', $result) && 
			   array_key_exists('stateid', $result) &&
			   array_key_exists('directionid', $result) &&
			   array_key_exists('balance', $result) &&
			   array_key_exists('percentageDiscount', $result) &&
			   array_key_exists('comment', $result)) {
				try{
    				$db = getConnection();
    				$stmt = $db->prepare('INSERT INTO Agents VALUES(\'\', :an, :stid, :did, :ba, :pd, 0)');
    				$stmt->bindParam(':an', $result->name);
    				$stmt->bindParam(':stid', $result->stateid);
        			$stmt->bindParam(':did', $result->directionid);
        			$stmt->bindParam(':ba', $result->balance);
					$stmt->bindParam(':pd', $result->percentageDiscount);
    				$stmt->execute();
    				
    				if($result->comment != "none"){
    					$stmt = $db->prepare('SELECT Agent_Id FROM Agents WHERE Agent_Name = :an');
    					$stmt->bindParam(':an', $result->name);
    					$stmt->execute();
    					
    					$result2 = $stmt->fetchAll(PDO::FETCH_ASSOC);
    					echo $result2[0]['Agent_Id'] . "\n";
    				}
    				$stmt = $db->prepare('INSERT INTO AgentNotes (AgentNote_Id, Agent_Id, AgentNotes_Comment)
	    				VALUES(NULL, :id, :com)
	    				ON DUPLICATE KEY UPDATE
	    				AgentNotes_Comment = :com');
						$stmt->bindParam(':id', $result2[0]['Agent_Id']);
						$stmt->bindParam(':com', $result->comment);
	    				$stmt->execute();

    				sendResponse(200, '{"Error":"0", "Message":"Successfully created agent"}');
    			}
		    	catch(PDOException $e){
    				sendResponse(400, '{"Error":"3002", "Message":"'. $e->getMessage() .'"}');
    			}
				return;	
			}
			sendResponse(400, '{"Error":"3001", "Message":"Appropriate agent keys not passed in"}');
	   		return;
    	}
    	sendResponse(400, '{"Error":"3000", "Message":"You must pass in agent data to filter on"}');
    	return;
   	}

	function editAgent(){
		$result = getJSONDecodedPostData();
		//This makes sure at least one of the key exists before intersecting the array with the master array
		//no point intersecting with the master array if one of the below keys don't even exist
		if($result != NULL){
			if(array_key_exists('id', $result) &&
			(array_key_exists('name', $result) ||
			//array_key_exists('lastname', $result) ||
			array_key_exists('stateid', $result) ||
			array_key_exists('balance', $result) ||
			array_key_exists('percentageDiscount', $result) ||
			array_key_exists('directionid', $result) ||
			array_key_exists('comment', $result))) {
				//
				try{
					$db = getConnection();	
					$array1 = get_object_vars($result);
    				$array2 = array('name'=>'','stateid'=>'','balance'=>'', 'percentageDiscount'=>'', 'directionid'=>'', 'comment'=>'');
    				
    				//$newArray;
    				$intersectionResult = array_intersect_key($array1, $array2);
    				
    				//firstname
    				if(array_key_exists('name', $result)){
    				
						$newArray["Agent_Name"] = $intersectionResult['name'];
    				}
    				
       				/*
//lastname
					if(array_key_exists('lastname', $result)){
    					$newArray['Agent_Lastname'] = $intersectionResult['lastname'];
    				}
    				
*/
    				//stateid
    				if(array_key_exists('stateid', $result)){
    					$newArray['State_Id'] = $intersectionResult['stateid'];
    				}
    				
    				//balance
    				if(array_key_exists('balance', $result)){
    					$newArray['Agent_Balance'] = $intersectionResult['balance'];
    				}
    				
    				//percentageDiscount
    				if(array_key_exists('percentageDiscount', $result)){
    					$newArray['Agent_PercentageDiscount'] = $intersectionResult['percentageDiscount'];
    				}
    				
    				//direction
    				if(array_key_exists('directionid', $result)){
    					$newArray['Direction_Id'] = $intersectionResult['directionid'];
    				}
    				if(isset($newArray)){
    		
						$sql = "UPDATE Agents SET ";
						foreach($newArray as $key => $value)
	    				$sql .= "$key = '$value',";
						$sql = substr($sql,0,-1);       //Remove the last comma
	    				//echo "sql : $sql \n\n";
	    				
	    				$stmt = $db->prepare($sql . '	 WHERE Agent_Id = :id');
	    				$stmt->bindParam(':id', $result->id);
	    				$stmt->execute();
	    				
	    				echo "\n\n sql: " . $sql;
	    				$stmt = $db->prepare('INSERT INTO AgentNotes (AgentNote_Id, Agent_Id, AgentNotes_Comment)
	    				VALUES(NULL, :id, :com)
	    				ON DUPLICATE KEY UPDATE
	    				AgentNotes_Comment = :com');
						$stmt->bindParam(':id', $result->id);
						$stmt->bindParam(':com', $result->comment);
	    				$stmt->execute();
		    			sendResponse(200, '{"Error":"0", "Message":"Successfully edited agent"}');
		    		}
		    		else{
		    		sendResponse(400, '{"Error":"3001", "Message":"Appropriate agent keys not passed in"}');
		    		}

    			}
		    	catch(PDOException $e){
/* 		    		echo "firstname: " . $result->firstname; */
    				sendResponse(400, '{"Error":"3002", "Message":"'. $e->getMessage() .'"}');
    				return;
    			}
				return;	
			}
			sendResponse(400, '{"Error":"3001", "Message":"Appropriate agent keys not passed in "' . json_encode($result). '"}');
	   		return;
    	}
    	sendResponse(400, '{"Error":"3000", "Message":"You must pass in agent data to filter on"}');
    	return;
	}

	function retrieveAllAgents(){
		try{
			$db = getConnection();
			//$stmt = $db->prepare('SELECT * FROM Factories');
			/*$stmt = $db->prepare('SELECT a.Agent_Id, a.Agent_Name, a.Agent_Balance, a.Agent_PercentageDiscount, b.State_Id, b.State_Name, b.State_AbbreviatedName, a.Direction_Id, c.Direction_Name, d.AgentNotes_Comment
								  FROM Agents a, States b, Directions c, AgentNotes d
								  WHERE a.State_Id = b.State_Id AND a.Direction_Id = c.Direction_Id AND a.Agent_Id = d.Agent_Id');
*/


			$stmt = $db->prepare('SELECT a.Agent_Id, a.Agent_Name, a.Agent_Balance, a.Agent_PercentageDiscount, a.Agent_OrderCount, b.State_Id, b.State_Name, b.State_AbbreviatedName, a.Direction_Id, c.Direction_Name, d.AgentNotes_Comment
			FROM States b, Directions c, Agents a LEFT OUTER JOIN AgentNotes d
			ON a.Agent_Id = d.Agent_Id
			WHERE a.State_Id = b.State_Id AND a.Direction_Id = c.Direction_Id');
			
			$stmt->execute();
			$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
			
			
	
			$stmt = $db->prepare('SELECT * FROM States');
			$stmt->execute();
			$result2 = $stmt->fetchAll(PDO::FETCH_ASSOC);
			$stmt->execute();

			
			$array = array(
				"agentsResults"=>$result,
				"stateResults"=>$result2		
			);
			sendResponse(200, json_encode($array));

		}
		catch(PDOException $e){
			sendResponse(400, '{"Error":"3002", "Message":"'. $e->getMessage() .'"}');
		}
	
	};
	
	
	
	function retrieveAgent(){
	    $result = getJSONDecodedPostData();
    	
    	if($result != NULL){
			if(array_key_exists('agentid', $result)) {
				$db = getConnection();
				
				$stmt = $db->prepare('	SELECT 
											a.Agent_Id, a.Agent_Name, a.Agent_Balance, a.Agent_PercentageDiscount, a.Agent_OrderCount, 
											b.State_Id, b.State_Name, b.State_AbbreviatedName, a.Direction_Id, c.Direction_Name, d.AgentNotes_Comment
										FROM 
											States b, 
											Directions c, 
											Agents a 
											
										LEFT OUTER JOIN 
											AgentNotes d
										ON 
											a.Agent_Id = d.Agent_Id
										WHERE
											a.State_Id = b.State_Id AND 
											a.Direction_Id = c.Direction_Id AND 
											a.Agent_Id = :aid');
											
											
				$stmt->bindParam(':aid', $result->agentid);
		
				$stmt->execute();
				$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
				echo sendResponse(200, json_encode($result));
				return;
			}
			sendResponse(400, '{"Error":"3001", "Message":"Appropriate \'agent\' keys not passed in"}');
	   		return;
    	}
    	sendResponse(400, '{"Error":"3000", "Message":"You must pass in \'agent\' data to filter on"}');
    	return;

	};
	


function deleteAgents(){
	    $result = getJSONDecodedPostData();
    	
    	if($result != NULL){
			if(array_key_exists('agentstodelete', $result)) {
				$array = $result->agentstodelete;
				$arrayOfRowsThatCouldntBeDeleted = array();
			
				if(!empty($array)){
						$db = getConnection();
					
					for($i = 0; $i < count($array); $i++){
						
						try{
							// DELETE FROM `Rajam`.`States` WHERE `States`.`State_ID` =18 
			    			$stmt = $db->prepare('DELETE FROM Agents WHERE Agent_Id = :aid AND Agent_Balance = 0');
			    			$stmt->bindParam(':aid', $array[$i]);
			    			$stmt->execute();
			    			
			    			
			    			$count = $stmt->rowCount();
			    			if($count == 0){
			    				array_push($arrayOfRowsThatCouldntBeDeleted, $array[$i]);
	
			    			}
			   				//sendResponse(200, '{"Error":"0", "Message":"Successfully deleted \'state\'"}');
						}
						catch(PDOException $e){
	
		    				sendResponse(400, '{"Error":"3002", "Message":"'. $e->getMessage() .'"}');
		    			}
		    			
						//echo $array[$i] ." ";
						//return;
					}
					$field_data = array();
					for($i = 0; $i < count($arrayOfRowsThatCouldntBeDeleted); $i++){
						$field_data[$i] = $arrayOfRowsThatCouldntBeDeleted[$i];
					}
					
					//print $array;
					//echo json_encode(array_keys($arrayOfRowsThatCouldntBeDeleted))	;
					echo sendResponse(200, json_encode($field_data));
					return;
				}
				else{
					sendResponse(400, '{"Error":"3001", "Message":"Empty \'agent\' data not allowed"}');
				}
			}
			sendResponse(400, '{"Error":"3001", "Message":"Appropriate \'agent\' keys not passed in"}');
	   		return;
    	}
    	sendResponse(400, '{"Error":"3000", "Message":"You must pass in \'agent\' data to filter on"}');
    	return;
   	}

	
	
////////////// END AGENTS
////////////////////////////////////////////////////////////////////

?>