<?php

////////////////////////////////////////////////////////////////////
////////////// BEGINNING FACTORIES

	function addFactory(){
	    $result = getJSONDecodedPostData();
    	
    	if($result != NULL){
			if(array_key_exists('factoryname', $result) && 
			   array_key_exists('stateid', $result)) {
				
				try{
    				$db = getConnection();
    				$stmt = $db->prepare('INSERT INTO Factories VALUES(\'\', :fn, :stid)');
    				$stmt->bindParam(':fn', $result->factoryname);
        			$stmt->bindParam(':stid', $result->stateid);
    				$stmt->execute();
    				sendResponse(200, '{"Error":"0", "Message":"Successfully created factory"}');
    			}
		    	catch(PDOException $e){
/* 		    		echo "firstname: " . $result->firstname; */
    				sendResponse(400, '{"Error":"3002", "Message":"'. $e->getMessage() .'"}');
    			}
				return;	
			}
			sendResponse(400, '{"Error":"3001", "Message":"Appropriate factory keys not passed in"}');
	   		return;
    	}
    	sendResponse(400, '{"Error":"3000", "Message":"You must pass in factory data to filter on"}');
    	return;
   	}
    	
	
	
	function editFactory(){
		$result = getJSONDecodedPostData();
		//This makes sure at least one of the key exists before intersecting the array with the master array
		//no point intersecting with the master array if one of the below keys don't even exist
		if($result != NULL){
			if(array_key_exists('id', $result) &&
			(array_key_exists('factoryname', $result) ||
			array_key_exists('stateid', $result))) {
				
				try{
					$db = getConnection();	
					$array1 = get_object_vars($result);
    				$array2 = array('factoryname'=>'','stateid'=>'');
    				
    				//$newArray;
    				$intersectionResult = array_intersect_key($array1, $array2);
    				
    				//factoryname
    				if(array_key_exists('factoryname', $result)){
    				
						$newArray["Factory_Name"] = $intersectionResult['factoryname'];
    				}
    				
       				//stateid
					if(array_key_exists('stateid', $result)){
    					$newArray['State_Id'] = $intersectionResult['stateid'];
    				}
   					$sql = "UPDATE Factories SET ";
					foreach($newArray as $key => $value)
    				$sql .= "$key = '$value',";
					$sql = substr($sql,0,-1);       //Remove the last comma
    				
    				$stmt = $db->prepare($sql . ' WHERE Factory_Id = :id');
    				$stmt->bindParam(':id', $result->id);
    				
    				$stmt->execute();
	    			sendResponse(200, '{"Error":"0", "Message":"Successfully edited factory"}');

    			}
		    	catch(PDOException $e){
/* 		    		echo "firstname: " . $result->firstname; */
    				sendResponse(400, '{"Error":"3002", "Message":"'. $e->getMessage() .'"}');
    				return;
    			}
				return;	
			}
			sendResponse(400, '{"Error":"3001", "Message":"Appropriate factory keys not passed in"}');
	   		return;
    	}
    	sendResponse(400, '{"Error":"3000", "Message":"You must pass in factory data to filter on"}');
    	return;
	}

	function retrieveAllFactories(){
		try{
			$db = getConnection();
			//$stmt = $db->prepare('SELECT * FROM Factories');
			$stmt = $db->prepare('SELECT a.Factory_Id, a.Factory_Name, b.State_Id, b.State_Name, b.State_AbbreviatedName
								  FROM Factories a, States b
								  WHERE a.State_Id = b.State_Id');

			$stmt->execute();
			$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
			
			
			$stmt = $db->prepare('SELECT * FROM States');
			$stmt->execute();
			$result2 = $stmt->fetchAll(PDO::FETCH_ASSOC);
			$stmt->execute();
			
			$array = array(
				"factoryResults"=>$result,
				"stateResults"=>$result2
			);
			
			sendResponse(200, json_encode($array));
		}
    	catch(PDOException $e){
			echo '{"Error":"3002", "Message":"'. $e->getMessage() .'"}';
		}
		return;	
	};	
	
	
		function deleteFactories(){
	    $result = getJSONDecodedPostData();
    	
    	if($result != NULL){
			if(array_key_exists('factoriestodelete', $result)) {
				$array = $result->factoriestodelete;
				$arrayOfRowsThatCouldntBeDeleted = array();
			
				$db = getConnection();

				for($i = 0; $i < count($array); $i++){
					
					try{
						// DELETE FROM `Rajam`.`States` WHERE `States`.`State_ID` =18 
		    			$stmt = $db->prepare('DELETE FROM Factories WHERE Factory_ID = :fid');
		    			$stmt->bindParam(':fid', $array[$i]);
		    			$stmt->execute();
		   				//sendResponse(200, '{"Error":"0", "Message":"Successfully deleted \'state\'"}');
					}
					catch(PDOException $e){
						array_push($arrayOfRowsThatCouldntBeDeleted, $array[$i]);

	    				//sendResponse(400, '{"Error":"3002", "Message":"'. $e->getMessage() .'"}');
	    			}
	    			
					//echo $array[$i] ." ";
					//return;
				}
				$field_data = array();
				for($i = 0; $i < count($arrayOfRowsThatCouldntBeDeleted); $i++){
					$field_data[$i] = $arrayOfRowsThatCouldntBeDeleted[$i];
				}
				
				//print $array;
				//echo json_encode(array_keys($arrayOfRowsThatCouldntBeDeleted))	;
				echo sendResponse(200, json_encode($field_data));
				return;

			}
			sendResponse(400, '{"Error":"3001", "Message":"Appropriate \'factory\' keys not passed in"}');
	   		return;
    	}
    	sendResponse(400, '{"Error":"3000", "Message":"You must pass in \'factory\' data to filter on"}');
    	return;
   	}
	

	
	
	
	
////////////// END FACTORIES
////////////////////////////////////////////////////////////////////


?>