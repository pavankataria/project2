<?php

	function authenticateUser(){
    	/*
$request = \Slim\Slim::getInstance()->request();
    	$postData = $request->getBody();
    	$result = json_decode($postData);
*/
    	
    	$result = getJSONDecodedPostData();
    	if($result != NULL){
			if(array_key_exists('username', $result) && 
			   array_key_exists('password', $result) &&
			   array_key_exists('ud', $result)) {
				
				try{
    				$db = getConnection();
    				/*SELECT `User_Username`
FROM `Users`
WHERE `User_Username` = 'jokes'
AND `User_Password` = 'mgod'
*/
    				$stmt = $db->prepare('SELECT * FROM Users WHERE User_Username = :un AND User_Password = :pw AND User_UDID = :ud');
       				$stmt->bindParam(':un', $result->username);
        			$stmt->bindParam(':pw', $result->password);
					$stmt->bindParam(':ud', $result->ud);

    				$stmt->execute();
    				
    				/*			$stmt->execute();
			$result = $stmt->fetch(PDO::FETCH_ASSOC);
			echo json_encode($result);
			*/
					$users = $stmt->fetchObject();
        			$db = null;
        			if($users == false){
						sendResponse(401, '{"Error":"3003", "Message":"Authentication failed"}');
						return;
        			}
        			sendResponse(200, json_encode($users));

    				//echo '{"Error":"0", "Message":"Successful"}';
    				return;
    			}
		    	catch(PDOException $e){
/* 		    		echo "firstname: " . $result->firstname; */
    				sendResponse(400, '{"Error":"3002", "Message":"'. $e->getMessage() .'"}');
    			}
				return;	
			}
			sendResponse(400, '{"Error":"3001", "Message":"Appropriate user keys not passed in"}');
	   		return;
    	}
    	sendResponse(400, '{"Error":"3000", "Message":"You must pass in user data to filter on"}');
    	return;
   	}
	
	
	function getUserForIpadWithUDID(){    	
    	$result = getJSONDecodedPostData();
    	if($result != NULL){
			if(array_key_exists('ud', $result)) {
				
				try{
    				$db = getConnection();

    				$stmt = $db->prepare('SELECT a.User_Username FROM Users a WHERE User_UDID = :ud');
					$stmt->bindParam(':ud', $result->ud);

    				$stmt->execute();
    				
   					$users = $stmt->fetchObject();
        			$db = null;
        			if($users == false){
						sendResponse(401, '{"Error":"3003", "Message":"Not a registered iPad. Reported to the IT Team"}');
						return;
        			}
        			sendResponse(200, json_encode($users));
    				return;
    			}
		    	catch(PDOException $e){
/* 		    		echo "firstname: " . $result->firstname; */
    				sendResponse(400, '{"Error":"3002", "Message":"'. $e->getMessage() .'"}');
    			}
				return;	
			}
			sendResponse(400, '{"Error":"3001", "Message":"Appropriate user keys not passed in"}');
	   		return;
    	}
    	sendResponse(400, '{"Error":"3000", "Message":"You must pass in user data to filter on"}');
    	return;
   	}


?>