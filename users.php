<?php
///////////////////////////////////////////////////
//////////////// BEGINNING USERS

	function createUser(){
	    $request = \Slim\Slim::getInstance()->request();
    	$postData = $request->getBody();
		$result = json_decode($postData, true);
    	
    	if($result != NULL){
			if(array_key_exists('firstname', $result) && 
			   array_key_exists('lastname', $result) &&
			   array_key_exists('username', $result) &&
			   array_key_exists('password', $result)) {
				
				try{
    				$db = getConnection();
    				$stmt = $db->prepare('INSERT INTO Users VALUES(\'\', :fn, :ln, :un, :pw)');
    				$stmt->bindParam(':fn', $result->firstname);
        			$stmt->bindParam(':ln', $result->lastname);
       				$stmt->bindParam(':un', $result->username);
        			$stmt->bindParam(':pw', $result->password);
    				$stmt->execute();
    				sendResponse(400, '{"Error":"0", "Message":"Successfully created user"}');
    			}
		    	catch(PDOException $e){
/* 		    		echo "firstname: " . $result->firstname; */
    				sendResponse(400,  '{"Error":"3002", "Message":"'. $e->getMessage() .'"}');
    			}
				return;	
			}
			sendResponse(400, '{"Error":"3001", "Message":"Appropriate user keys not passed in"}');
	   		return;
    	}
    	sendResponse(400, '{"Error":"3000", "Message":"You must pass in user data to filter on"}');
    	return;
   	}

	function editUsers(){
		$result = getJSONDecodedPostData();
		//This makes sure at least one of the key exists before intersecting the array with the master array
		//no point intersecting witht he master array if one of the below keys don't even exist
		if($result != NULL){
			if(array_key_exists('id', $result) && (array_key_exists('firstname', $result) || 
			   array_key_exists('lastname', $result) ||
			   array_key_exists('username', $result) ||
			   array_key_exists('password', $result))) {
				
				try{
					$db = getConnection();	
					$array1 = get_object_vars($result);
    				$array2 = array('firstname'=>'','lastname'=>'','username'=>'','password'=>'');
    				
    				//$newArray;
    				$intersectionResult = array_intersect_key($array1, $array2);
    				
    				if(array_key_exists('firstname', $result)){
    				
						$newArray["User_Firstname"] = $intersectionResult['firstname'];
    				}
    				
					if(array_key_exists('lastname', $result)){
    					$newArray['User_Lastname'] = $intersectionResult['lastname'];
    				}
    				if(array_key_exists('username', $result)){
    					$newArray['User_Username'] = $intersectionResult['username'];
    				}
    				if(array_key_exists('password', $result)){
    					$newArray['User_Password'] = $intersectionResult['password'];
    				}
    				    				
    				//print_r ($intersectionResult);

					$sql = "UPDATE Users SET ";
					foreach($newArray as $key => $value)
    				$sql .= "$key = '$value',";
					$sql = substr($sql,0,-1);       //Remove the last comma
    				
    				$stmt = $db->prepare($sql . '	 WHERE User_Id = :id');
    				$stmt->bindParam(':id', $result->id);
    				//echo $sql . ' WHERE User_Idsds = lklk' . $result->id;

/*     				TO DO LIST REPLACE KEYS WITH APPROPRIATE KEY NAMES */

    				$stmt->execute();
	    			echo '{"Error":"0", "Message":"Successfully edited user"}';

    			}
		    	catch(PDOException $e){
/* 		    		echo "firstname: " . $result->firstname; */
    				echo '{"Error":"3002", "Message":"'. $e->getMessage() .'"}';
    				return;
    			}
				return;	
			}
			sendResponse(400, '{"Error":"3001", "Message":"Appropriate user keys not passed in"}');
	   		return;
    	}
    	sendResponse(400, '{"Error":"3000", "Message":"You must pass in user data to filter on"}');
    	return;
	}
	
	function retrieveAllUsers(){
		try{
			$db = getConnection();
			$stmt = $db->prepare('SELECT * FROM Users WHERE ');
			$stmt->execute();
			$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
			echo json_encode($result);
		}
    	catch(PDOException $e){
			echo '{"Error":"3002", "Message":"'. $e->getMessage() .'"}';
		}
		return;	
	};
	
	
	/*

	function retrieveAllUsers(){
		try{
			$db = getConnection();
			$stmt = $db->prepare('SELECT * FROM Users');
			$stmt->execute();
			$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
			echo json_encode($result);
		}
    	catch(PDOException $e){
			echo '{"Error":"3002", "Message":"'. $e->getMessage() .'"}';
		}
		return;	
	};

*/

////////////// END USERS
////////////////////////////////////////////////////////////////////

?>