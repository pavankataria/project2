<?php

function createPayment(){
	$result = getJSONDecodedPostData();
		//This makes sure at least one of the key exists before intersecting the array with the master array
		//no point intersecting with the master array if one of the below keys don't even exist
		if($result != NULL){
			if(array_key_exists('id', $result) &&
			(array_key_exists('pa', $result) ||
			//array_key_exists('lastname', $result) ||
			array_key_exists('tr', $result) ||
			array_key_exists('ts', $result) ||
			array_key_exists('ad', $result))) {
				try{
					$db = getConnection();	
					$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
					$db->beginTransaction();
					
					
					
					//STEP 1) LOGGING THE AGENT ACCOUNT
					//Sort out time input for india
					date_default_timezone_set('Asia/Calcutta');
					$script_tz = date_default_timezone_get();
					//$date = date('d/m/Y h:i:s A');
					$date = date('Y-m-d H:i:s');
				
					$array1 = get_object_vars($result);    				
					$array2 = array('pa'=>'','tr'=>'', 'ts'=>'', 'ad'=>'',);
				    				
    				//$newArray;
    				$intersectionResult = array_intersect_key($array1, $array2);
	
	
					$totalPaymentAmount = 0;
					foreach($intersectionResult as $key => $value){
					    if($key == 'pa'){
						    $stmt = $db->prepare('INSERT INTO Payments VALUES(NULL, 1, :aid, :am, :date)');
						}
						else if($key == 'tr'){
						    $stmt = $db->prepare('INSERT INTO Payments VALUES(NULL, 2, :aid, :am, :date)');
						}
						else if($key == 'ts'){
						    $stmt = $db->prepare('INSERT INTO Payments VALUES(NULL, 3, :aid, :am, :date)');
						}
						else if($key == 'ad'){
						    $stmt = $db->prepare('INSERT INTO Payments VALUES(NULL, 4, :aid, :am, :date)');
						}
						$totalPaymentAmount += $value;
						
						$stmt->bindParam(':aid', $result->id);
						$stmt->bindParam(':am', $value);
						$stmt->bindParam(':date', $date);
	    				$stmt->execute();
					}
					
					/*
echo "totalPaymentAmount: " . $totalPaymentAmount;
					
					echo "agent id: " . $result->id;
*/
					
					
					//STEP 2) GET ALL ORDERS FROM SPECIFIC AGENTS WHOSE ORDERS ARE STATE 4
					$stmt = $db->prepare('SELECT * FROM Orders Where Agent_Id = :aid AND Order_State = 4');
					$stmt->bindParam(':aid', $result->id);
					$stmt->execute();
					$allOrdersState4 = $stmt->fetchAll(PDO::FETCH_ASSOC);
					
					//STEP 3) DETERMINE IF THERE's ANY DIFFERENCE BETWEEN TOLB (Total Order Level Balance) AND CALB (Current Agent Level Balance)
					//If so that is our PB (whatever that stands for… something to do with debt left over (i made the word and i don't even know -_-)); 
					//PENDING BALANCE
					
					$TOLB = 0;
					
					for($i = 0; $i < count($allOrdersState4); $i++){
						$TOLB += $allOrdersState4[$i]['Order_LevelBalance'];
					}
				/*
	echo "TOTAL LEVEL BALANCE: " . $TOLB;
					
					echo "	Total agent amount: " . $totalPaymentAmount;
					
					
					
					//Print database order result
					echo "
					
					
					
					
					database order result: " . JSON_ENCODE($allOrdersState4);
*/
					
					
					//Check if the Agent has an PB and if so.. deduct from there first with the amount paid
					$stmt = $db->prepare('SELECT * FROM Agents WHERE Agent_Id = :aid');
					$stmt->bindParam(':aid', $result->id);
					$stmt->execute();
					$agentBalanceRowResult = $stmt->fetchAll(PDO::FETCH_ASSOC);
					
					
					
					$agentBalance = $agentBalanceRowResult[0]['Agent_Balance'];
					
					$agentWishesToPayTotalAmount = $totalPaymentAmount;
					
					$totalOrderLevelBalance = $TOLB;
					
					
					
					
					
					if(abs($agentBalance) > $totalOrderLevelBalance){
						$difference = abs($agentBalance)-$totalOrderLevelBalance;
						
						//if the payment amount is created than the pending balance… then take difference from payment amount and put that into affect for order
						if($totalPaymentAmount > $difference){
						
							
							$agentWishesToPayTotalAmount = $totalPaymentAmount-$difference;

							
							
							for($j = 0; $j < count($allOrdersState4); $j++){
								$currentOLB = $allOrdersState4[$j]['Order_LevelBalance'];
								//echo "array Order OLB: " .  $currentOLB . "     ";
								//If the OLD debt can be cleared because the current order amount is less than the amount paid by agent then close the current order under analysis there and then 
								if($currentOLB <= $agentWishesToPayTotalAmount){
									$agentWishesToPayTotalAmount -= $currentOLB;
									
								
									//echo "total = " . $agentWishesToPayTotalAmount . " after deducting amount " . $currentOLB . " from " . $;
									//Close the order
									$stmt = $db->prepare('UPDATE Orders SET Order_State = 5, Order_LevelBalance = 0 WHERE Order_ReferenceNumber = :orn AND Agent_Id = :aid  AND Order_State = 4');
									$stmt->bindParam(':orn', $allOrdersState4[$j]['Order_ReferenceNumber']);
									$stmt->bindParam(':aid', $result->id);
									
									$stmt->execute();
								}
								
								else if($currentOLB > $agentWishesToPayTotalAmount){
									$finalOrderBalance = $currentOLB - $agentWishesToPayTotalAmount;
									echo "fob: " . $finalOrderBalance . " COB: " . $currentOLB . " agentWishesTPA: " . $agentWishesToPayTotalAmount . "      ";
									$stmt = $db->prepare('UPDATE Orders SET Order_State = 4, Order_LevelBalance = :fob WHERE Order_ReferenceNumber = :orn AND Agent_Id = :aid  AND Order_State = 4');
									$stmt->bindParam(':orn', $allOrdersState4[$j]['Order_ReferenceNumber']);
									$stmt->bindParam(':aid', $result->id);
									$stmt->bindParam(':fob', $finalOrderBalance);
		
									
									$stmt->execute();
									//echo "             ";
		
								}
								else{
								}
							}

						
						}
					}
					else{
						//Go through each order and check what the order level balance is and then subtract from each order level balance from each order
						
						for($j = 0; $j < count($allOrdersState4); $j++){
							$currentOLB = $allOrdersState4[$j]['Order_LevelBalance'];
							//echo "array Order OLB: " .  $currentOLB . "     ";
							//If the OLD debt can be cleared because the current order amount is less than the amount paid by agent then close the current order under analysis there and then 
							if($currentOLB <= $agentWishesToPayTotalAmount){
								$agentWishesToPayTotalAmount -= $currentOLB;
								
							
								//echo "total = " . $agentWishesToPayTotalAmount . " after deducting amount " . $currentOLB . " from " . $;
								//Close the order
								$stmt = $db->prepare('UPDATE Orders SET Order_State = 5, Order_LevelBalance = 0 WHERE Order_ReferenceNumber = :orn AND Agent_Id = :aid  AND Order_State = 4');
								$stmt->bindParam(':orn', $allOrdersState4[$j]['Order_ReferenceNumber']);
								$stmt->bindParam(':aid', $result->id);
								
								$stmt->execute();
							}
							
							else if($currentOLB > $agentWishesToPayTotalAmount){
								$finalOrderBalance = $currentOLB - $agentWishesToPayTotalAmount;
								echo "fob: " . $finalOrderBalance . " COB: " . $currentOLB . " agentWishesTPA: " . $agentWishesToPayTotalAmount . "      ";
								$stmt = $db->prepare('UPDATE Orders SET Order_State = 4, Order_LevelBalance = :fob WHERE Order_ReferenceNumber = :orn AND Agent_Id = :aid  AND Order_State = 4');
								$stmt->bindParam(':orn', $allOrdersState4[$j]['Order_ReferenceNumber']);
								$stmt->bindParam(':aid', $result->id);
								$stmt->bindParam(':fob', $finalOrderBalance);
	
								
								$stmt->execute();
								//echo "             ";
	
							}
							else{
							}
						}
					}
						
					$finalBalance = $agentBalance + $totalPaymentAmount;


					$stmt = $db->prepare('UPDATE Agents SET Agent_Balance = :ab WHERE Agent_Id = :aid');
					$stmt->bindParam(':ab', $finalBalance);
					$stmt->bindParam(':aid', $result->id);
					$stmt->execute();

					

					
					
					$db->commit();

					sendResponse(200, '{"Error":"0", "Message":"Successfully added payments"}');

					return;

				}    			
				catch(PDOException $e){
				
					$db->rollBack();

					sendResponse(400, '{"Error":"3002", "Message":"'. $e->getMessage() .'"}');
					return;
				}
				return;
			}
			sendResponse(400, '{"Error":"3001", "Message":"Appropriate agent keys not passed in"}');//' . json_encode($result). '"}');
	   		return;
    	}
    	sendResponse(400, '{"Error":"3000", "Message":"You must pass in agent data to filter on"}');
    	return;

   }
   

function retrieveAllAgentAccounts(){
	$result = getJSONDecodedPostData();

	if($result != NULL){
		if(array_key_exists('id', $result)){
			try{
				$db = getConnection();
				$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				$db->beginTransaction();				

				$stmt = $db->prepare('SELECT * FROM Orders Where Agent_Id = :id AND (Order_State = 4 || Order_State = 5)');
				$stmt->bindParam(':id', $result->id);
				$stmt->execute();
				$result1 = $stmt->fetchAll(PDO::FETCH_ASSOC);
				
				
/* 				$stmt = $db->prepare('SELECT * FROM Payments WHERE Payment_AgentId = :id'); */
				$stmt = $db->prepare('SELECT a.*, b.PaymentType_Name FROM Payments a, PaymentTypes b WHERE a.PaymentType_Id = b.PaymentType_Id AND a.Payment_AgentId = :id');

				$stmt->bindParam(':id', $result->id);
				
				$stmt->execute();
				$result2 = $stmt->fetchAll(PDO::FETCH_ASSOC);
				$stmt->execute();
	
				$stmt = $db->prepare('SELECT Agent_Balance FROM Agents WHERE Agent_Id = :aid');// .*, b.PaymentType_Name FROM Payments a, PaymentTypes b WHERE a.PaymentType_Id = b.PaymentType_Id AND a.Payment_AgentId = :id');

				$stmt->bindParam(':aid', $result->id);
				
				$stmt->execute();
				$result3 = $stmt->fetchAll(PDO::FETCH_ASSOC);
				$stmt->execute();
				
				
				$array = array(
					"orderResults"=>$result1,
					"paymentResults"=>$result2,
					"agentBalance"=>$result3

				);
				
				$db->commit();
				sendResponse(200, json_encode($array));
			}
	    	catch(PDOException $e){
				$db->rollBack();
				echo '{"Error":"3002", "Message":"'. $e->getMessage() .'"}';
			}
			return;	
		}
		sendResponse(400, '{"Error":"3001", "Message":"Appropriate agent keys not passed in"}');//' . json_encode($result). '"}');
   		return;

	}
	sendResponse(400, '{"Error":"3000", "Message":"You must pass in agent data to filter on"}');
	return;

}	
	






	
/*
$db = getConnection();	
					$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
					$db->beginTransaction();
					

					$array1 = get_object_vars($result);
    				
					$array2 = array('pa'=>'','tr'=>'', 'ts'=>'', 'ad'=>'',);
				    				
    				//$newArray;
    				$intersectionResult = array_intersect_key($array1, $array2);
 
    				
    				echo "count: ".count($intersectionResult);
    				
    				

					foreach($intersectionResult as $key => $value){
					    if($key == 'pa'){
						    $stmt = $db->prepare('INSERT INTO Payments VALUES(NULL, 1, :aid, :am)';
						}
						else if($key == 'tr'){
						    $stmt = $db->prepare('INSERT INTO Payments VALUES(NULL, 2, :aid, :am)';
						}
						else if($key == 'ts'){
						    $stmt = $db->prepare('INSERT INTO Payments VALUES(NULL, 3, :aid, :am)';
						}
						else if($key == 'ad'){
						    $stmt = $db->prepare('INSERT INTO Payments VALUES(NULL, 4, :aid, :am)';
						}

						$stmt->bindParam(':aid', $result->id);
						$stmt->bindParam(':am', $value);
	    				$stmt->execute();
	    				$result3 = $stmt->fetchAll(PDO::FETCH_ASSOC);
						echo "";
*/
    				

?>	