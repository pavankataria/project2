<?php
	/*
////////////////////////////////////////////////////////////////////
////////////// BEGINNING STATE
*/
	
	function addState(){
	    $result = getJSONDecodedPostData();
    	
    	if($result != NULL){
			if(array_key_exists('statename', $result) && 
			   array_key_exists('abbreviation', $result)) {
				
				
				
				try{
    				$db = getConnection();
    				$stmt = $db->prepare('INSERT INTO States VALUES(\'\', :sn, :ab)');
    				$stmt->bindParam(':sn', $result->statename);
        			$stmt->bindParam(':ab', $result->abbreviation);
/* 					$stmt->bindParam(':lo', $result->listingorder); */
    				$stmt->execute();
    				sendResponse(200, '{"Error":"0", "Message":"Successfully created \'state\'"}');
    			}
		    	catch(PDOException $e){
    				sendResponse(400,
    				 '{"Error":"3002", "Message":"'. $e->getMessage() .'"}');
    			}
				return;	
			}
			sendResponse(400, '{"Error":"3001", "Message":"Appropriate \'state\' keys not passed in"}');
	   		return;
    	}
    	sendResponse(400, '{"Error":"3000", "Message":"You must pass in \'state\' data to filter on"}');
    	return;
   	}
   	
   	
   	   	
   	
   	function deleteStates(){
	    $result = getJSONDecodedPostData();
    	
    	if($result != NULL){
			if(array_key_exists('statestodelete', $result)) {
				$array = $result->statestodelete;
				$arrayOfRowsThatCouldntBeDeleted = array();
			
				$db = getConnection();

				for($i = 0; $i < count($array); $i++){
					
					try{
						// DELETE FROM `Rajam`.`States` WHERE `States`.`State_ID` =18 
		    			$stmt = $db->prepare('DELETE FROM States WHERE State_ID = :sid');
		    			$stmt->bindParam(':sid', $array[$i]);
		    			$stmt->execute();
		   				//sendResponse(200, '{"Error":"0", "Message":"Successfully deleted \'state\'"}');
					}
					catch(PDOException $e){
						array_push($arrayOfRowsThatCouldntBeDeleted, $array[$i]);

	    				//sendResponse(400, '{"Error":"3002", "Message":"'. $e->getMessage() .'"}');
	    			}
	    			
					//echo $array[$i] ." ";
					//return;
				}
				$field_data = array();
				for($i = 0; $i < count($arrayOfRowsThatCouldntBeDeleted); $i++){
					$field_data[$i] = $arrayOfRowsThatCouldntBeDeleted[$i];
				}
				
				//print $array;
				//echo json_encode(array_keys($arrayOfRowsThatCouldntBeDeleted))	;
				echo sendResponse(200, json_encode($field_data));
				return;

			}
			sendResponse(400, '{"Error":"3001", "Message":"Appropriate \'state\' keys not passed in"}');
	   		return;
    	}
    	sendResponse(400, '{"Error":"3000", "Message":"You must pass in \'state\' data to filter on"}');
    	return;
   	}
	
   	
   	
	
	function editState(){
		$result = getJSONDecodedPostData();
		//This makes sure at least one of the key exists before intersecting the array with the master array
		//no point intersecting with the master array if one of the below keys don't even exist
		if($result != NULL){
			if(array_key_exists('id', $result) &&
			(array_key_exists('statename', $result) ||
			array_key_exists('abbreviation', $result))) {
				
				try{
					$db = getConnection();	
					$array1 = get_object_vars($result);
    				$array2 = array('statename'=>'','abbreviation'=>'');
    				
    				//$newArray;
    				$intersectionResult = array_intersect_key($array1, $array2);
    				
    				//factoryname
    				if(array_key_exists('statename', $result)){
    				
						$newArray["State_Name"] = $intersectionResult['statename'];
    				}
    				
       				//stateid
					if(array_key_exists('abbreviation', $result)){
    					$newArray['State_AbbreviatedName'] = $intersectionResult['abbreviation'];
    				}
    				
   					$sql = "UPDATE States SET ";
					foreach($newArray as $key => $value)
    				$sql .= "$key = '$value',";
					$sql = substr($sql,0,-1);       //Remove the last comma
    				
    				$stmt = $db->prepare($sql . ' WHERE State_Id = :id');
    				$stmt->bindParam(':id', $result->id);
    				
    				$stmt->execute();
	    			sendResponse(200, '{"Error":"0", "Message":"Successfully edited \'state\'"}');
    			}
		    	catch(PDOException $e){
    				sendResponse(400, '{"Error":"3002", "Message":"'. $e->getMessage() .'"}');
    				return;
    			}
				return;	
			}
			sendResponse(400, '{"Error":"3001", "Message":"Appropriate \'state\' keys not passed in"}');
	   		return;
    	}
    	sendResponse(400, '{"Error":"3000", "Message":"You must pass in \'state\' data to filter on"}');
    	return;
	}

	function retrieveAllStates(){
		try{
			$db = getConnection();
			$stmt = $db->prepare('SELECT * FROM States');
			$stmt->execute();
			$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
			sendResponse(200, json_encode($result));
		}
    	catch(PDOException $e){
			sendResponse(400, '{"Error":"3002", "Message":"'. $e->getMessage() .'"}');
		}
		return;	
	};	
////////////// END STATE
////////////////////////////////////////////////////////////////////
?>